const StringUtils = (function() {
    const preDynamicText = new ccui.Text();
    preDynamicText.retain();
    preDynamicText.setTextHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
    preDynamicText.setTextVerticalAlignment(cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
    function getTextSize(text, fontName, fontSize) {
        preDynamicText.setFontName(fontName);
        preDynamicText.setFontSize(fontSize);
        preDynamicText.setString(text);
        return preDynamicText.getContentSize();
    }
    return {
        getTextSize,
    };
})();
