let InjectCCS = {
    ctor: function () {
        this._super();
    },

    defineCE: function (arr) {
        this._ceArr = arr;
    },
    getCE: function (name) {
        return this[name];
    },
    injectElement: function (path) {
        let mainScene = ccs.load(path, "");
        this._rootNode = mainScene.node;
        this.addChild(mainScene.node);

        for (let i = 0; i < this._ceArr.length; i += 1) {
            let ceName = this._ceArr[i];
            this.recursiveSearchChild(mainScene.node, ceName);
        }
    },
    recursiveSearchChild: function (node, name) {
        if (node.getName() === name) {
            // cc.log("InjectCCS | recursiveSearchChild | element: " + name);
            this[name] = node;
            if (name.indexOf("btn") >= 0) {
                node.addTouchEventListener(this.touchEventHandle.bind(this), node);
            }
        }
        else {
            var arr = node.getChildren();
            for (var i = 0; i < arr.length; i += 1) this.recursiveSearchChild(arr[i], name);
        }
    },
    touchEventHandle: function (sender, type) {
        switch (type)
        {
            case ccui.Widget.TOUCH_BEGAN:
                this.onTouchBeganEvent(sender);
                break;
            case ccui.Widget.TOUCH_MOVED:
                this.onTouchMovedEvent(sender);
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.onTouchEndedEvent(sender);
                break;
            case ccui.Widget.TOUCH_CANCELED:
                this.onTouchCancelledEvent(sender);
                break;
            default:
                break;
        }
    },

    // method to override
    onTouchBeganEvent: function (sender) {},
    onTouchMovedEvent: function (sender) {},
    onTouchEndedEvent: function (sender) {},
    onTouchCancelledEvent: function (sender) {},
};
