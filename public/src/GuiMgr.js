let GuiMgr = {
    getGUI: function (guiClass) {
        if (!this._guiArr) return null;
        for (let i = 0; i < this._guiArr.length; i += 1) {
            var gui = this._guiArr[i];
            if (gui instanceof guiClass) return gui;
        }
        return null;
    },
    viewGUI: function (guiClass) {
        if (this.getGUI(guiClass)) {
            cc.warn("GuiMgr | viewGUI | gui already added");
            return;
        }

        var gui = new guiClass();
        if (!this._guiArr) this._guiArr = [];
        this._guiArr.push(gui);
        cc.director.getRunningScene().addChild(gui);

        gui._rootNode.setOpacity(0);
        gui._rootNode.runAction(cc.fadeIn(0.1));
        return gui;
    },
    removeGUI: function (guiClass) {
        let gui = this.getGUI(guiClass);
        if (gui) {
            var index = this._guiArr.indexOf(gui);
            this._guiArr.splice(index, 1);

            gui._rootNode.runAction(cc.sequence(
                cc.fadeOut(0.1),
                cc.callFunc(() => {
                    gui.removeFromParent();
                })
            ));
        }
    }
};
