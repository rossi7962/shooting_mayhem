let GunGameConf = {
    loadJson: function () {
        if (!this._json) {
            cc.loader.loadJson("res/json/gungame.json", (error, data) => {
                this._json = data;
            });
        }
    },
    getLevelArray: function () {
        return this._json["level"];
    },
};

let BattleMgr = cc.Node.extend({
    scene: null,
    ctor: function (scene) {
        this.scene = scene;
        this._super();

        this.setButtonSettings(SettingsData.getKeySettingsData());
        this.setPressedKeyCodeArray([]);
        this.setReleasedKeyCodeArray([]);

        let eventKeyboardListener = cc.EventListener.create({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: (keyCode, event) => {
                let arr = this.getPressedKeyCodeArray();
                if (arr.indexOf(keyCode) < 0) arr.push(keyCode);

                if (keyCode === 27) {
                    this.pauseGame();
                    let guiPause = GuiMgr.viewGUI(GuiPauseGame);
                    if (guiPause) {
                        guiPause.setResumeCallback(() => {
                            this.resumeGame();
                            GuiMgr.removeGUI(GuiPauseGame);
                        });
                        guiPause.setQuitCallback(() => {
                            GuiMgr.removeGUI(SceneBattle);
                            GuiMgr.removeGUI(GuiPauseGame);
                        });
                    }
                }
            },
            onKeyReleased: (keyCode, event) => {
                let arr = this.getReleasedKeyCodeArray();
                if (arr.indexOf(keyCode) < 0) arr.push(keyCode);

                let pressedArr = this.getPressedKeyCodeArray();
                let index = pressedArr.indexOf(keyCode);
                pressedArr.splice(index, 1);
            }
        });

        cc.eventManager.addListener(eventKeyboardListener, this);
    },

    setTimeExecute: function (v) {
        this._timeExecute = v;
    },
    getTimeExecute: function () {
        return this._timeExecute;
    },
    setCharacterDataArr: function (v) {
        this._characterDataArray = v;
    },
    getCharacterDataArr: function () {
        return this._characterDataArray;
    },

    setMode: function (v) {
        this._gameMode = v;
    },
    getMode: function () {
        return this._gameMode;
    },
    setMapId: function (v) {
        this._mapId = v;
    },
    getMapId: function () {
        return this._mapId;
    },

    update: function (dt) {
        let time = this.getTimeExecute();
        time += dt;
        this.setTimeExecute(time);

        let isGunGame = this.getMode() === RoomConst.ROOM_MODE.GUN_GAME;

        let scene = this.scene;
        scene.getCharacterSpriteArray().forEach((sprCharacter, index) => {
            let elementData = this.getCharacterDataArr()[index];
            if (elementData["death"]) return;

            let characterDataElement = scene.getCharacterDataElementArray()[index];
            if (elementData["ammo"] === 0 && GunConfig.isHandgun(elementData["gun_id"]) && !isGunGame) {
                let gunData = GunConfig.getGunDataById(elementData["gun_id"]);
                let gap = this.getTimeExecute() - sprCharacter.getStampReload();
                let reloadTime = gunData["reload_time"];
                let percent = gap > reloadTime ? 100 : gap / reloadTime * 100;

                sprCharacter.updateReloadStatus(percent);
                if (gap > reloadTime) {
                    elementData["ammo"] = gunData["ammo"];
                    characterDataElement.presentData();
                }
            }

            let spdX = sprCharacter.getSpeedX();
            let spdY = sprCharacter.getSpeedY();
            let updateValueX = spdX * dt;
            let updateValueY = spdY * dt;
            sprCharacter.x += updateValueX;
            sprCharacter.y += updateValueY;

            if (sprCharacter.y < 0) {
                let lives = elementData["lives"] - 1;
                elementData["lives"] = lives < 0 ? 0 : lives;
                scene.showSomeOneKill(sprCharacter);

                if (!isGunGame) {
                    if (lives < 0) {
                        elementData["death"] = true;
                        this.tryEndGame();
                    } elementData["gun_id"] = elementData["handgun_id"];
                } else {
                    let lastHitData = sprCharacter.getLastHitData();
                    if (lastHitData) {
                        let arr = GunGameConf.getLevelArray();
                        let idx = lastHitData.userIndex;

                        let sprKill = scene.getCharacterSpriteArray()[idx];
                        let dataKiller = this.getCharacterDataArr()[idx];
                        let uiKiller = scene.getCharacterDataElementArray()[idx];

                        let nextLevel = dataKiller["level"] + 1;
                        if (nextLevel >= arr.length) this.tryEndGame();
                        else {
                            dataKiller["level"] = nextLevel;
                            dataKiller["gun_id"] = arr[nextLevel];
                            dataKiller["ammo"] = GunConfig.getGunDataById(dataKiller["gun_id"])["ammo"];
                            sprKill.pickUpGun(dataKiller["gun_id"]);

                            uiKiller.presentDataPickUpGun();
                            uiKiller.presentData();
                        }
                    }
                }

                elementData["ammo"] = GunConfig.getGunDataById(elementData["gun_id"])["ammo"];
                elementData["grenade_available"] = GrenadeConfig.getGrenadeDataById(elementData["grenade"])["number_available"];
                characterDataElement.presentDataPickUpGun();
                characterDataElement.presentData();
                sprCharacter.pickUpGun(elementData["gun_id"]);

                this.spawnCharacter(index);
                sprCharacter.setLastHitData(null);
                return;
            }

            let updateSpdY = this.handleFallingObject(sprCharacter, dt, updateValueY);
            spdY += updateSpdY;

            let keyPressedArr = this.getPressedKeyCodeArray();
            keyPressedArr.forEach((keyCode) => {
                let keyData = this.getButtonSettings()[index];
                if (!keyData) return;
                switch (keyCode) {
                    case keyData.UP:
                        if (!sprCharacter.getFalling()) {
                            spdY += BattleMgr.CHARACTER_SETTINGS.JUMP_SPEED;
                            sprCharacter.setFalling(true);
                            sprCharacter.setLineData(null);
                            sprCharacter.actionJump();
                        }
                        break;
                    case keyData.DOWN:
                        if (!sprCharacter.getFalling()) {
                            sprCharacter.setFalling(true);
                            sprCharacter.setLineData(null);
                        }
                        break;
                    case keyData.LEFT:
                        sprCharacter.getCE("sprCharacter").setScaleX(-1);
                        sprCharacter.setCharacterDirection(BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT);

                        if (spdX > -BattleMgr.CHARACTER_SETTINGS.MAX_SPEED) spdX -= BattleMgr.CHARACTER_SETTINGS.BASE_ACCELERATION * dt;
                        break;
                    case keyData.RIGHT:
                        sprCharacter.getCE("sprCharacter").setScaleX(1);
                        sprCharacter.setCharacterDirection(BattleMgr.BULLET_SETTINGS.DIRECTION.TO_RIGHT);

                        if (spdX < BattleMgr.CHARACTER_SETTINGS.MAX_SPEED) spdX += BattleMgr.CHARACTER_SETTINGS.BASE_ACCELERATION * dt;
                        break;
                    case keyData.FIRE:
                        let stamp = sprCharacter.getLastFireTimeStamp();
                        let now = this.getTimeExecute();
                        let gunData = GunConfig.getGunDataById(elementData["gun_id"]);
                        let delay = gunData["attack_delay"];
                        let positionData = BattleMgr.GUN_SETTINGS[elementData["gun_id"]];

                        if (now - stamp > delay) {
                            if (elementData["ammo"] > 0) {

                                // fire
                                let fire = 1;
                                if (gunData["distance"] <= GunConfig.getShotGunDistance()) fire = 5;
                                let starterY = positionData["start_fire_y"];
                                let gap = 16;
                                if (fire !== 1) starterY -= (gap * 2);

                                for (let i = 0; i < fire; i += 1) {
                                    let bullet = scene.getFreeBullet();
                                    if (bullet) {
                                        let direction = sprCharacter.getCharacterDirection();
                                        if (direction === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT)
                                            bullet.x = -positionData["start_fire_x"] + sprCharacter.x;
                                        else
                                            bullet.x = +positionData["start_fire_x"] + sprCharacter.x;
                                        bullet.y = starterY + sprCharacter.y - 8 + Math.random() * 16;
                                        starterY += gap;
                                        scene.fireBullet(bullet, direction, gunData["damage"], gunData["distance"], index, elementData["gun_id"]);
                                    }
                                }

                                // update data
                                elementData["ammo"] -= 1;
                                if (elementData["ammo"] === 0) {
                                    if (gunData["reload_time"] > 0) sprCharacter.setStampReload(this.getTimeExecute());
                                    else {
                                        if (!isGunGame) {
                                            elementData["gun_id"] = elementData["handgun_id"];
                                            sprCharacter.pickUpGun(elementData["gun_id"]);
                                        }
                                    }
                                }
                                characterDataElement.presentDataPickUpGun();
                                characterDataElement.presentData();
                                sprCharacter.setLastFireTimeStamp(this.getTimeExecute());
                                sprCharacter.actionFireBullet();
                            }
                        }
                        break;
                    case keyData.NADE:
                        if (!isGunGame) {
                            let stampGrenade = sprCharacter.getLastThrowGrenadeStamp();
                            let timeNow = this.getTimeExecute();
                            let data = GrenadeConfig.getGrenadeDataById(elementData["grenade"]);
                            let delayNade = data["delay"];
                            if (timeNow - stampGrenade > delayNade) {
                                if (elementData["grenade_available"] > 0) {
                                    let grenade = scene.getFreeGrenade(elementData["grenade"]);
                                    if (grenade) {
                                        let direction = sprCharacter.getCharacterDirection();
                                        grenade.x = sprCharacter.x;
                                        grenade.y = sprCharacter.y + 40;
                                        grenade.setStampThrown(this.getTimeExecute());
                                        scene.throwGrenadeOnMap(grenade, direction, index);
                                    }

                                    elementData["grenade_available"] -= 1;
                                    sprCharacter.setLastThrowGrenadeStamp(this.getTimeExecute());
                                }
                            }
                        }
                        break;
                }
            });

            let keyReleasedArr = this.getReleasedKeyCodeArray();
            for (let i = 0; i < keyReleasedArr.length; i += 1) {
                let keyCode = keyReleasedArr.pop();
                let keyData = this.getButtonSettings()[index];
                if (!keyData) continue;
                switch (keyCode) {
                    case keyData.UP:
                        break;
                    case keyData.DOWN:
                        break;
                    case keyData.LEFT:
                        break;
                    case keyData.RIGHT:
                        break;
                    case keyData.FIRE:
                        break;
                    case keyData.NADE:
                        break;
                    default:
                        keyReleasedArr.push(keyCode);
                        break;
                }
            }

            // help user break when press nothing
            let keyData = this.getButtonSettings()[index];
            let counterAccelerate = 0;
            if (keyData) {
                if (keyPressedArr.indexOf(keyData.RIGHT) < 0 && keyPressedArr.indexOf(keyData.LEFT) < 0) {
                    counterAccelerate = BattleMgr.CHARACTER_SETTINGS.BASE_ACCELERATION * dt / 2;
                }
            }
            if (!keyData) {
                counterAccelerate = BattleMgr.CHARACTER_SETTINGS.BASE_ACCELERATION * dt / 2;
            }

            if (spdX < 0) {
                spdX += counterAccelerate;
                if (spdX > 0) spdX = 0;
            } else {
                spdX -= counterAccelerate;
                if (spdX < 0) spdX = 0;
            }

            scene.getActiveBulletArray().forEach((bullet) => {
                if (bullet.getActive()) {
                    if (this.getMode() === RoomConst.ROOM_MODE.LAST_TEAM_STANDING) {
                        let userIdx = bullet.getFromUserIndex();
                        let userData = this.getCharacterDataArr()[userIdx];
                        if (elementData["team"] === userData["team"]) return;
                    }

                    let subPos = cc.pSub(bullet, sprCharacter);
                    if (Math.abs(subPos.x) < (bullet.width + sprCharacter.getBoundWidthSpr()) / 2 &&
                        subPos.y < (bullet.height + sprCharacter.getBoundHeightSpr()) && subPos.y > 0) {
                        sprCharacter.runEffectHit();
                        sprCharacter.setLastHitData({
                            userIndex: bullet.getFromUserIndex(),
                            weaponId: bullet.getGunId(),
                            damageDealerType: DAMAGE_DEALER_TYPE.GUN,
                        });

                        let damageTakenX = bullet.getDamage() * BattleMgr.BULLET_SETTINGS.SCALE_DAMAGE;
                        if (bullet.getBulletDirection() === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT) damageTakenX = -damageTakenX;
                        spdX += damageTakenX;

                        scene.showBloodHit(sprCharacter.x, bullet.y);
                        for (let i = 0; i < 5; i += 1) scene.showBloodFly(sprCharacter.x, bullet.y, bullet.getBulletDirection());
                        scene.stopBullet(bullet);
                    }
                }
            });

            scene.getActiveBoxArray().forEach((box) => {
                if (box.getActive()) {
                    let subPos = cc.pSub(box, sprCharacter);
                    if (Math.abs(subPos.x) < (box.width + sprCharacter.getBoundWidthSpr()) / 2 &&
                        subPos.y <= sprCharacter.getBoundHeightSpr() && subPos.y >= -box.height) {
                        elementData["gun_id"] = box.getGunId();
                        elementData["ammo"] = GunConfig.getGunDataById(box.getGunId())["ammo"];
                        characterDataElement.presentData();
                        characterDataElement.presentDataPickUpGun();
                        sprCharacter.pickUpGun(box.getGunId());
                        sprCharacter.updateReloadStatus(100);
                        scene.destroyBox(box);
                    }
                }
            });

            sprCharacter.setSpeedX(spdX);
            sprCharacter.setSpeedY(spdY);
        });

        scene.getActiveBulletArray().forEach((bullet) => {
            let direction = bullet.getBulletDirection();
            let moved = BattleMgr.BULLET_SETTINGS.SPEED * dt;
            if (direction === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT) bullet.x -= moved;
            else bullet.x += moved;
            bullet.setDistance(bullet.getDistance() - moved);
            if (bullet.getDistance() < 0) scene.stopBullet(bullet);
        });

        if (!isGunGame) {
            let timeGap = this.getTimeExecute() - scene.getStampDropBox();
            if (timeGap > BattleMgr.MAP_SETTINGS.TIME_SPAWN_BOX) scene.dropBoxOnMap();
            scene.getActiveBoxArray().forEach((box) => {
                let mapConfig = MapConfig.getMapDataById(this.getMapId());
                let lines = mapConfig["lines"];

                for (let i = lines.length - 1; i >= 0; i -= 1) {
                    let lineData = lines[i];
                    if (lineData["start_x"] < box.x + box.width / 2
                        && lineData["end_x"] > box.x - box.width / 2) {
                        if (box.y <= lineData["y"]) box.y = lineData["y"];
                        else box.y -= BattleMgr.CHARACTER_SETTINGS.GRAVITY * dt;
                        break;
                    }
                }
            });
        }

        scene.getActiveGrenadeArray().forEach((grenade) => {
            let spdX = grenade.getSpeedX();
            let spdY = grenade.getSpeedY();
            let updateValueX = spdX * dt;
            let updateValueY = spdY * dt;
            grenade.x += updateValueX;
            grenade.y += updateValueY;
            grenade.setSpeedY(spdY + this.handleFallingObject(grenade, dt, updateValueY));

            if (!grenade.getFalling()) {
                let counterAccelerate = BattleMgr.GRENADE_SETTINGS.FRICTION_ACCELERATION * dt;
                if (spdX < 0) {
                    spdX += counterAccelerate;
                    if (spdX > 0) spdX = 0;
                } else {
                    spdX -= counterAccelerate;
                    if (spdX < 0) spdX = 0;
                }
            }

            grenade.setSpeedX(spdX);

            let stampThrown = grenade.getStampThrown();
            let timeExecute = this.getTimeExecute();
            let dataConf = GrenadeConfig.getGrenadeDataById(grenade.getId());
            let explodeAfter = dataConf["explode_after"];
            if (timeExecute - stampThrown >= explodeAfter) {
                // explode

                if (dataConf["type"] === "GRENADE") {
                    scene.getCharacterSpriteArray().forEach((sprCharacter) => {
                        let range = dataConf["range_radius"];
                        let highestDamage = dataConf["highest_damage"] ? dataConf["highest_damage"] : 0;
                        let lowestDamage = dataConf["lowest_damage"] ? dataConf["lowest_damage"] : 0;
                        let subPos = cc.pSub(sprCharacter, grenade);
                        let subDistance = cc.pDistance(grenade, sprCharacter);
                        if (Math.abs(subDistance) < range) {
                            if (dataConf["type"] === "GRENADE") {
                                let gapScale = highestDamage - lowestDamage;
                                let damageDeal = lowestDamage + (1 - subDistance / range) * gapScale;

                                let scaleDamageX = subPos.x / subDistance;
                                let damageX = damageDeal * scaleDamageX;

                                let scaleDamageY = subPos.y / subDistance;
                                let damageY = damageDeal * scaleDamageY;

                                let spdCharX = sprCharacter.getSpeedX();
                                let spdCharY = sprCharacter.getSpeedY();
                                // cc.log("damage deal to " + sprCharacter.getData()["name"] + " is x: " + damageX + " y: " + damageY + " total: " + damageDeal);

                                if (damageY!== 0) {
                                    sprCharacter.setFalling(true);
                                    sprCharacter.setLineData(null);
                                }
                                sprCharacter.setSpeedX(spdCharX + damageX);
                                sprCharacter.setSpeedY(spdCharY + damageY);

                                sprCharacter.setLastHitData({
                                    userIndex: grenade.getThrownFrom(),
                                    weaponId: grenade.getId(),
                                    damageDealerType: DAMAGE_DEALER_TYPE.GRENADE,
                                });
                            }
                        }
                    });

                    scene.stopGrenade(grenade);
                    scene.showExplosive(grenade);
                } else if (dataConf["type"] === "SMOKE") {
                    if (!grenade.getSmoke()) scene.showSmoke(grenade);
                } else if (dataConf["type"] === "STUN") {
                    scene.stopGrenade(grenade);
                    scene.showFlash(grenade);
                }
            }

            if (timeExecute - (stampThrown + explodeAfter) >= dataConf["duration"]) {
                if (dataConf["type"] === "SMOKE") {
                    let smoke = grenade.getSmoke();
                    smoke.stopSmokeOnScene();
                    grenade.setSmoke(null);
                    scene.stopGrenade(grenade);
                }
            }
        });
    },
    handleFallingObject: function (obj, dt, updateValueY) {
        let scene = this.scene;
        let lines = MapConfig.getMapDataById(this.getMapId())["lines"];
        let updateSpeedY = 0;
        if (obj.getFalling()) {
            for (let i = lines.length - 1; i >= 0; i -= 1) {
                let lineData = lines[i];
                if (lineData["y"] < obj.y - updateValueY && obj.getSpeedY() < 0 && lineData["y"] > obj.y) {
                    if (lineData["start_x"] < obj.x + obj.getBoundWidthSpr() / 2
                        && lineData["end_x"] > obj.x - obj.getBoundWidthSpr() / 2) {
                        obj.setFalling(false);

                        // negate the fall
                        updateSpeedY = -obj.getSpeedY();
                        obj.setLineData({
                            "start_x": lineData["start_x"],
                            "end_x": lineData["end_x"],
                            "y": lineData["y"]
                        });
                        obj.y = lineData["y"];
                        obj.actionLanding();
                        for (let i = 0; i < 10; i += 1) scene.showDust(obj.x, obj.y);
                        break;
                    }
                }
            }

            if (obj.getFalling()) {
                updateSpeedY = -BattleMgr.CHARACTER_SETTINGS.GRAVITY * dt;
            }
        } else {
            let lineData = obj.getLineData();
            if (lineData["start_x"] > obj.x + obj.getBoundWidthSpr() / 2
                || lineData["end_x"] < obj.x - obj.getBoundWidthSpr() / 2) {
                obj.setFalling(true);
                obj.setLineData(null);
            }
        }

        return updateSpeedY;
    },
    spawnCharacter: function (index) {
        let scene = this.scene;

        scene.getCharacterSpriteArray()[index].x = BattleMgr.CHARACTER_SETTINGS.RESPAWN_POSITION.x;
        scene.getCharacterSpriteArray()[index].y = BattleMgr.CHARACTER_SETTINGS.RESPAWN_POSITION.y;
        scene.getCharacterSpriteArray()[index].stopRunning();
        scene.getCharacterSpriteArray()[index].resetStat();
    },
    startGame: function () {
        let scene = this.scene;
        scene.getCharacterSpriteArray().forEach((spr, index) => {
            this.spawnCharacter(index);
        });

        this.setTimeExecute(0);
        this.scheduleUpdate();
    },
    pauseGame: function () {
        this.unscheduleUpdate();
    },
    resumeGame: function () {
        this.scheduleUpdate();
    },
    tryEndGame: function () {
        switch (this.getMode()) {
            case RoomConst.ROOM_MODE.LAST_TEAM_STANDING:
                let haveTeamA = !!this.getCharacterDataArr().some((item) => {
                    return item["team"] === RoomConst.TEAM.A && !item["death"];
                });
                let haveTeamB = !!this.getCharacterDataArr().some((item) => {
                    return item["team"] === RoomConst.TEAM.B && !item["death"];
                });
                if (!haveTeamA || !haveTeamB) {
                    this.unscheduleUpdate();
                    this.scene.showGameOver();
                }
                break;
            case RoomConst.ROOM_MODE.LAST_MAN_STANDING:
                let arr = this.getCharacterDataArr();
                let aliveArr = arr.filter((item) => {
                    return item["death"] === false;
                });
                if (aliveArr.length <= 1) {
                    this.unscheduleUpdate();
                    this.scene.showGameOver();
                }
                break;
            case RoomConst.ROOM_MODE.GUN_GAME:
                this.unscheduleUpdate();
                this.scene.showGameOver();
                break;
        }
    },

    setButtonSettings: function (v) {
        this._buttonSettings = v;
    },
    getButtonSettings: function () {
        return this._buttonSettings;
    },
    setPressedKeyCodeArray: function (v) {
        this._pressedKeyCodeArray = v;
    },
    getPressedKeyCodeArray: function () {
        return this._pressedKeyCodeArray;
    },
    setReleasedKeyCodeArray: function (v) {
        this._releasedKeyCodeArray = v;
    },
    getReleasedKeyCodeArray: function () {
        return this._releasedKeyCodeArray;
    }
});


BattleMgr.GUN_SETTINGS = {
    // DESERT EAGLE
    0: {
        x: 80,
        y: 32,
        start_fire_x: 80,
        start_fire_y: 41
    },

    // GLOCK 18
    1: {
        x: 80,
        y: 32,
        start_fire_x: 80,
        start_fire_y: 41
    },

    // MP5
    2: {
        x: 41,
        y: 29,
        start_fire_x: 58,
        start_fire_y: 41
    },

    // Steyr TMP
    3: {
        x: 82,
        y: 29,
        start_fire_x: 94,
        start_fire_y: 41
    },

    // M4A1
    4: {
        x: 46,
        y: 29,
        start_fire_x: 86,
        start_fire_y: 36
    },

    // AK47
    5: {
        x: 50,
        y: 32,
        start_fire_x: 94,
        start_fire_y: 41
    },

    // Spass 12
    6: {
        x: 86,
        y: 36,
        start_fire_x: 130,
        start_fire_y: 40
    },

    // Gatling Gun
    7: {
        x: 70,
        y: 8,
        start_fire_x: 112,
        start_fire_y: 6
    },

    // AWM
    8: {
        x: 64,
        y: 28,
        start_fire_x: 118,
        start_fire_y: 40
    },

    // Schmidt Scout
    9: {
        x: 60,
        y: 36,
        start_fire_x: 108,
        start_fire_y: 37
    }
};

BattleMgr.CHARACTER_SETTINGS = {
    RESPAWN_POSITION: {
        x: 960,
        y: 1080
    },
    FRICTION_ACCELERATION_SCALE: 80,
    BASE_ACCELERATION: 1000,
    MAX_SPEED: 300,
    GRAVITY: 1000,
    JUMP_SPEED: 450,
    SHOCK_DURATION: 0.3
};

BattleMgr.BULLET_SETTINGS = {
    DIRECTION: {
        TO_LEFT: 0,
        TO_RIGHT: 1,
    },
    SPEED: 800,
    SCALE_DAMAGE: 1
};

BattleMgr.MAP_SETTINGS = {
    TIME_SPAWN_BOX: 15
};

BattleMgr.GRENADE_SETTINGS = {
    START_SPEED_X: 200,
    START_SPEED_Y: 300,
    FRICTION_ACCELERATION: 400,
};

let battleMgr = {};
