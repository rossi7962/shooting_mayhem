let GrenadeConfig = {
    loadJson: function () {
        if (!this._json) {
            cc.loader.loadJson("res/json/grenade.json", (error, data) => {
                cc.log("GunConfig: " + data);
                this._json = data;
            });
        }
    },
    getGrenadeArray: function () {
        return this._json["grenade"];
    },
    getGrenadeDataById: function (id) {
        return this._json["grenade"].find((item) => {
            return item["id"] === id
        })
    },
    getGrenadePathById: function (id) {
        switch (id) {
            case GrenadeConst.ID.EXPLOSIVE_GRENADE:
                return "sprites/grenade/grenade.png";
            case GrenadeConst.ID.STUN_GRENADE:
                return "sprites/grenade/stun_grenade.png";
            case GrenadeConst.ID.SMOKE_GRENADE:
                return "sprites/grenade/smoke_grenade.png";
        }
    }
};

let GrenadeConst = {
    ID: {
        EXPLOSIVE_GRENADE: 0,
        STUN_GRENADE: 1,
        SMOKE_GRENADE: 2
    }
};