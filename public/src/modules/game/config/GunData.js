let GunConfig = {
    loadJson: function () {
        if (!this._json) {
            cc.loader.loadJson("res/json/gun.json", (error, data) => {
                cc.log("MapConfig: " + data);
                this._json = data;
            });
        }
    },
    isHandgun: function (gunId) {
        let data = this.getGunDataById(gunId);
        return data["type"] === GunConst.TIER.HAND_GUN;
    },
    getGunArray: function () {
        return this._json["gun"];
    },
    getGunDataById: function (id) {
        return this._json["gun"].find((item) => {
            return item["id"] === id
        })
    },
    getGunHaveHighestStatField: function (field) {
        let arr = this.getGunArray();
        let returnItem = {};
        returnItem[field] = -999999;
        arr.forEach((item) => {
            if (item[field] > returnItem[field]) returnItem = item;
        });

        return returnItem;
    },
    getShotGunDistance: function () {
        return this._json["shotgun_distance"];
    },
    getGunPathById: function (id) {
        switch (id) {
            case GunConst.TYPE.DESERT_EAGLE:
                return "sprites/gun/gun_desert_eagle.png";
            case GunConst.TYPE.GLOCK_18:
                return "sprites/gun/gun_glock.png";
            case GunConst.TYPE.MP5:
                return "sprites/gun/gun_mp5.png";
            case GunConst.TYPE.STEYR_TMP:
                return "sprites/gun/gun_steyr_tmp.png";
            case GunConst.TYPE.M4A1:
                return "sprites/gun/gun_m4a1.png";
            case GunConst.TYPE.AK47:
                return "sprites/gun/gun_ak47.png";
            case GunConst.TYPE.SPASS_12:
                return "sprites/gun/gun_spass_12.png";
            case GunConst.TYPE.GATLING_GUN:
                return "sprites/gun/gun_gatling_gun.png";
            case GunConst.TYPE.AWM:
                return "sprites/gun/gun_awm.png";
            case GunConst.TYPE.SCHMIDT_SCOUT:
                return "sprites/gun/gun_schdmit_scout.png";
        }
    }
};

let GunConst = {
    TIER : {
        HAND_GUN: 0,
        SHOT_GUN: 1,
        SMG: 2,
        RIFLE: 3,
        MG: 4,
    },
    TYPE: {
        DESERT_EAGLE: 0,
        GLOCK_18: 1,
        MP5: 2,
        STEYR_TMP: 3,
        M4A1: 4,
        AK47: 5,
        SPASS_12: 6,
        GATLING_GUN: 7,
        AWM: 8,
        SCHMIDT_SCOUT: 9
    }
}
