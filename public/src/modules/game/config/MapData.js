let MapData = {

};

let MapConfig = {
    loadJson: function () {
        if (!this._json) {
            cc.loader.loadJson("res/json/map.json",  (error, data) => {
                this._json = data;
            });
        }
    },
    getMapArray: function () {
        return this._json["map"];
    },
    getMapDataById: function (id) {
        return this._json["map"].find((mapItem) => {
            return mapItem["id"] === id
        })
    },
    getMapPathById: function (id) {
        return "sprites/map/map_" + id + ".png";
    }
};
