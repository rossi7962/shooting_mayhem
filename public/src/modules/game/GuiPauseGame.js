let GuiPauseGame = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnResume",
            "btnEndGame"
        ]);
        this.injectElement("zccs/game/GuiPauseGame.json");
    },
    setResumeCallback: function (v) {
        this._resumeCallback = v;
    },
    setQuitCallback: function (v) {
        this._quitCallback = v;
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnResume"):
                this._resumeCallback();
                break;
            case this.getCE("btnEndGame"):
                this._quitCallback();
                break;
        }
    }
});
