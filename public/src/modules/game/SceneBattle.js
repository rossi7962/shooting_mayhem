let SceneBattle = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "sprMap",
            "ndUserData",
            "lbGameOver",
            "ndTextKill"
        ]);
        this.injectElement("zccs/game/SceneBattle.json");
        this.init();
    },
    init: function () {
        battleMgr = new BattleMgr(this);
        battleMgr.setMode(RoomData.getRoomMode());
        battleMgr.setMapId(RoomData.getMapId());
        this.addChild(battleMgr);

        this._characterArr = [];
        this._characterDataElementArr = [];
        let posX = 0;
        let characterDataArray = [];
        RoomData.getUserArr().forEach((data) => {
            if (data["active"] === RoomConst.ACTIVE_TYPE.NONE) return;

            let item = new CharacterDataElement;
            if (RoomData.getRoomMode() === RoomConst.ROOM_MODE.GUN_GAME) item = new CharacterDataElementGunGame;
            let itemData = {
                "name": data["name"],
                "character_type": data["character_type"],
                "handgun_id": data["gun"],
                "grenade": data["grenade"],
                "grenade_available": GrenadeConfig.getGrenadeDataById(data["grenade"])["number_available"],
                "gun_id": data["gun"],
                "death": false,
                "lives": 10, // gun game does not use this field
                "level": 0, // gun game only
                "team": data["team"],
                "ammo": GunConfig.getGunDataById(data["gun"])["ammo"]
            };
            characterDataArray.push(itemData);

            item.setData(itemData);
            if (RoomData.getRoomMode() === RoomConst.ROOM_MODE.GUN_GAME) {
                const arr = GunGameConf.getLevelArray();
                itemData["gun_id"] = arr[0];
                itemData["ammo"] = GunConfig.getGunDataById(itemData["gun_id"])["ammo"]
            }
            item.presentData();
            item.presentDataPickUpGun();
            item.presentDataFixed();
            item.x = posX;
            posX += CharacterDataElement.SETTINGS.SIZE.WIDTH;

            this._characterDataElementArr.push(item);
            this.getCE("ndUserData").addChild(item);

            let characterSprite = new CharacterSpriteBattle;
            characterSprite.pickUpGun(itemData["gun_id"]);
            characterSprite.setData({
                "name": data["name"],
                "character_type": data["character_type"],
                "team": data["team"]
            });
            characterSprite.presentData();
            if (battleMgr.getMode() === RoomConst.ROOM_MODE.LAST_TEAM_STANDING) characterSprite.presentTeam();

            this._characterArr.push(characterSprite);
            this.getCE("sprMap").addChild(characterSprite);
        });

        battleMgr.setCharacterDataArr(characterDataArray);

        this._activeBulletArray = [];
        this._bulletArray = [];
        for (let i = 0; i < 1000; i += 1) {
            let bullet = new Bullet("sprites/map/bullet.png");
            bullet.setActive(false);
            this._bulletArray.push(bullet);
            this.getCE("sprMap").addChild(bullet);
        }

        let sprMap = this.getCE("sprMap");
        sprMap.setOpacity(0);
        let path = MapConfig.getMapPathById(RoomData.getMapId());
        sprMap.setTexture(path);
        sprMap.runAction(cc.fadeIn(0.3));

        this._boxArray = [];
        this._activeBoxArray = [];
        for (let i = 0; i < 10; i += 1) {
            let box = new SupplyBox;
            box.setActive(false);
            box.setAnchorPoint(cc.p(0.5, 0));
            this._boxArray.push(box);
            this.getCE("sprMap").addChild(box);
        }

        this._dustArray = [];
        for (let i = 0; i < 100; i += 1) {
            let dust = new DustEff;
            dust.setActive(false);
            dust.setAnchorPoint(cc.p(0.5, 0));
            switch (battleMgr.getMapId()) {
                case 1:
                    dust.setColor(cc.color('#51300b'));
            }
            this._dustArray.push(dust);
            this.getCE("sprMap").addChild(dust);
        }

        this._bloodHitArray = [];
        for (let i = 0; i < 100; i += 1) {
            let bloodHit = new BloodHitEff;
            bloodHit.setActive(false);
            this._bloodHitArray.push(bloodHit);
            this.getCE("sprMap").addChild(bloodHit);
        }

        this._bloodFlyArray = [];
        for (let i = 0; i < 500; i += 1) {
            let bloodFly = new BloodFlyEff;
            bloodFly.setActive(false);
            this._bloodFlyArray.push(bloodFly);
            this.getCE("sprMap").addChild(bloodFly);
        }

        this._nodeTextKillArray = [];
        this._nodeTextKillActiveArray = [];
        for (let i = 0; i < 50; i += 1) {
            let ndTextKill = new NodeTextKill;
            ndTextKill.setActive(false);
            this._nodeTextKillArray.push(ndTextKill);
            this.getCE("ndTextKill").addChild(ndTextKill);
            ndTextKill.x = -2000;
        }

        this._explodeGrenadeArray = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new SpriteGrenade(GrenadeConst.ID.EXPLOSIVE_GRENADE);
            explode.setActive(false);
            this._explodeGrenadeArray.push(explode);
            this.getCE("sprMap").addChild(explode);
        }
        this._stunGrenadeArray = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new SpriteGrenade(GrenadeConst.ID.STUN_GRENADE);
            explode.setActive(false);
            this._stunGrenadeArray.push(explode);
            this.getCE("sprMap").addChild(explode);
        }
        this._smokeGrenadeArray = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new SpriteGrenade(GrenadeConst.ID.SMOKE_GRENADE);
            explode.setActive(false);
            this._smokeGrenadeArray.push(explode);
            this.getCE("sprMap").addChild(explode);
        }
        this._activeGrenadeArray = [];

        this._explodeDust = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new NodeExplode();
            explode.setActive(false);
            this._explodeDust.push(explode);
            this.getCE("sprMap").addChild(explode);
        }
        this._flashDust = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new NodeFlash();
            explode.setActive(false);
            this._flashDust.push(explode);
            this.getCE("sprMap").addChild(explode);
        }
        this._smokeDust = [];
        for (let i = 0; i < 50; i += 1) {
            let explode = new NodeSmoke();
            explode.setActive(false);
            this._smokeDust.push(explode);
            this.getCE("sprMap").addChild(explode);
        }

        this.setStampDropBox(0);
        battleMgr.startGame();
    },

    getCharacterSpriteArray: function () {
        return this._characterArr;
    },
    getCharacterDataElementArray: function () {
        return this._characterDataElementArr;
    },

    getBulletArray: function () {
        return this._bulletArray;
    },
    getFreeBullet: function () {
        for (let i = 0; i < this.getBulletArray().length; i += 1) {
            let bullet = this.getBulletArray()[i];
            if (!bullet.getActive()) return bullet;
        }
        return null;
    },
    fireBullet: function (bullet, direction, damage, distance, idx, gunId) {
        let arr = this.getActiveBulletArray();
        arr.push(bullet);

        bullet.setBulletDirection(direction);
        bullet.setDamage(damage);
        bullet.setDistance(distance);
        bullet.setActive(true);
        bullet.setFromUserIndex(idx);
        bullet.setGunId(gunId);

        let dir = direction === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT ? -1 : 1;
        bullet.setScaleX(dir);
        bullet.setOpacity(255);
    },
    stopBullet: function (bullet) {
        let arr = this.getActiveBulletArray();
        bullet.setOpacity(0);
        bullet.setActive(false);
        let index = arr.indexOf(bullet);
        arr.splice(index, 1);
    },
    getActiveBulletArray: function () {
        return this._activeBulletArray;
    },

    getFreeGrenade: function (grenadeId) {
        let arr;
        switch (grenadeId) {
            case GrenadeConst.ID.EXPLOSIVE_GRENADE:
                arr = this._explodeGrenadeArray;
                break;
            case GrenadeConst.ID.STUN_GRENADE:
                arr = this._stunGrenadeArray;
                break;
            case GrenadeConst.ID.SMOKE_GRENADE:
                arr = this._smokeGrenadeArray;
                break;
        }

        for (let i = 0; i < arr.length; i += 1) {
            let grenade = arr[i];
            if (!grenade.getActive()) return grenade;
        }

        cc.log("SceneBattle | getFreeGrenade | return NULL");
        return null;
    },
    getActiveGrenadeArray: function () {
        return this._activeGrenadeArray;
    },
    throwGrenadeOnMap: function (grenade, direction, userIdx) {
        let arr = this.getActiveGrenadeArray();
        arr.push(grenade);

        grenade.setOpacity(255);
        grenade.setActive(true);
        grenade.setFalling(true);
        grenade.setDirection(direction);
        grenade.setThrownFrom(userIdx);
        let num = direction === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT ? -1 : 1;
        grenade.setSpeedX(num * BattleMgr.GRENADE_SETTINGS.START_SPEED_X);
        grenade.setSpeedY(BattleMgr.GRENADE_SETTINGS.START_SPEED_Y);
    },
    stopGrenade: function (grenade) {
        let arr = this.getActiveGrenadeArray();
        grenade.setOpacity(0);
        grenade.setActive(false);
        let index = arr.indexOf(grenade);
        arr.splice(index, 1);
    },

    getBoxArray: function () {
        return this._boxArray;
    },
    getActiveBoxArray: function () {
        return this._activeBoxArray;
    },
    dropBoxOnMap: function () {
        this.setStampDropBox(battleMgr.getTimeExecute());
        for (let i = 0; i < this.getBoxArray().length; i += 1) {
            let box = this.getBoxArray()[i];
            if (!box.getActive()) {
                let arr = this.getActiveBoxArray();
                arr.push(box);
                box.setActive(true);

                let mapConfig = MapConfig.getMapDataById(battleMgr.getMapId());
                let gap = mapConfig["edge"]["right"] - mapConfig["edge"]["left"];

                box.x = mapConfig["edge"]["left"] + Math.floor(Math.random() * Math.floor(gap));
                box.y = BattleMgr.CHARACTER_SETTINGS.RESPAWN_POSITION.y;
                box.stopAllActions();
                box.runAction(cc.fadeIn(0.1));

                let gunArr = GunConfig.getGunArray();
                gunArr = gunArr.filter((item) => {
                    return item["type"] !== GunConst.TIER.HAND_GUN;
                });
                let index = Math.floor(Math.random() * Math.floor(gunArr.length));
                box.setGunId(gunArr[index]["id"]);
                return;
            }
        }
    },
    destroyBox: function (box) {
        let arr = this.getActiveBoxArray();
        let index = arr.indexOf(box);
        arr.splice(index, 1);
        box.stopAllActions();
        box.runAction(cc.fadeOut(0.1));
        box.setActive(false);
    },
    setStampDropBox: function (v) {
        this._stampDropBox = v;
    },
    getStampDropBox: function () {
        return this._stampDropBox;
    },

    showDust: function (x, y) {
        let arr = this._dustArray;
        for (let i = 0; i < arr.length; i += 1) {
            let dust = arr[i];
            if (!dust.getActive()) {
                dust.x = x;
                dust.y = y;
                dust.setActive(true);
                dust.setScale(Math.random());
                let direct = Math.random() > 0.5 ? 1 : -1;
                dust.runAction(cc.moveBy(0.3, Math.random() * 60 * direct, 0).easing(cc.easeOut(2)));
                dust.runAction(cc.sequence(cc.fadeIn(0.016), cc.fadeOut(0.3), cc.callFunc(() => {
                    dust.setActive(false);
                })));
                return;
            }
        }
    },
    showExplosive: function (grenade) {
        let arr = this._explodeDust;
        for (let i = 0; i < arr.length; i += 1) {
            let dust = arr[i];
            if (!dust.getActive()) {
                dust.setActive(true);
                dust.runActionExplode(grenade);
                return;
            }
        }
    },
    showFlash: function (grenade) {
        let arr = this._flashDust;
        for (let i = 0; i < arr.length; i += 1) {
            let dust = arr[i];
            if (!dust.getActive()) {
                dust.setActive(true);
                dust.runActionFlash(grenade);
                return;
            }
        }
    },
    showSmoke: function (grenade) {
        let arr = this._smokeDust;
        for (let i = 0; i < arr.length; i += 1) {
            let dust = arr[i];
            if (!dust.getActive()) {
                dust.setActive(true);
                dust.runActionShowSmokeOnScene(grenade);
                grenade.setSmoke(dust);
                return;
            }
        }
    },

    showBloodHit: function (x, y) {
        let arr = this._bloodHitArray;
        for (let i = 0; i < arr.length; i += 1) {
            let bloodHit = arr[i];
            if (!bloodHit.getActive()) {
                bloodHit.x = x;
                bloodHit.y = y;
                bloodHit.setActive(true);
                bloodHit.runAction(cc.sequence(cc.fadeIn(0.016), cc.fadeOut(0.3), cc.callFunc(() => {
                    bloodHit.setActive(false);
                })));
                return;
            }
        }
    },
    showBloodFly: function (x, y, direction) {
        let arr = this._bloodFlyArray;
        for (let i = 0; i < arr.length; i += 1) {
            let bloodFly = arr[i];
            if (!bloodFly.getActive()) {
                bloodFly.x = x;
                bloodFly.y = y;
                bloodFly.setScale(Math.random());
                bloodFly.setActive(true);

                let deltaX = Math.random() * 100;
                let deltaY = -50 + Math.random() * 100;
                if (direction === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_RIGHT) deltaX = -deltaX;

                bloodFly.runAction(cc.moveBy(0.3, deltaX, deltaY).easing(cc.easeOut(2)));
                bloodFly.runAction(cc.sequence(cc.fadeIn(0.016), cc.fadeOut(0.3), cc.callFunc(() => {
                    bloodFly.setActive(false);
                })));
                return;
            }
        }
    },
    showSomeOneKill: function (sprCharacter) {
        let lastHitData = sprCharacter.getLastHitData();
        let indexKiller = lastHitData ? lastHitData.userIndex : -1;
        let killerSprite = this.getCharacterSpriteArray()[indexKiller];
        let matchMode = battleMgr.getMode();

        for (let i = 0; i < this._nodeTextKillArray.length; i += 1) {
            let node = this._nodeTextKillArray[i];
            if (!node.getActive()) {
                this._nodeTextKillActiveArray.push(node);
                this.alignTextKill();

                node.setActive(true);
                let range = node.presentKillData(sprCharacter, killerSprite, matchMode);
                node.x = -range;

                node.runAction(cc.sequence(
                    cc.moveBy(0.6, range, 0).easing(cc.easeBackOut()),
                    cc.delayTime(5),
                    cc.moveBy(0.6, -range, 0).easing(cc.easeBackIn()),
                    cc.callFunc(() => {
                        node.x = -2000;
                        node.setActive(false);
                        this._nodeTextKillActiveArray.splice(this._nodeTextKillActiveArray.indexOf(node), 1);
                        this.alignTextKill();
                    }),
                ));
                return;
            }
        }
    },
    alignTextKill: function () {
        let space = 80;
        let starterY = 0;
        this._nodeTextKillActiveArray.forEach((node) => {
            node.y = starterY;
            starterY -= 80;
        });
    },

    showGameOver: function () {
        let lb = this.getCE("lbGameOver");
        lb.setOpacity(0);
        lb.setPosition(cc.p(cc.winSize.width / 2, cc.winSize.height / 2));
        lb.runAction(cc.sequence(
            cc.fadeIn(0.3),
            cc.delayTime(3),
            cc.callFunc(() => {
                GuiMgr.removeGUI(SceneBattle);
            })
        ));
    },
});

let Bullet = cc.Sprite.extend({
    ctor: function (path) {
        this._super(path);
    },

    setDistance: function (v) {
        this._distance = v;
    },
    getDistance: function () {
        return this._distance;
    },

    setGunId: function (gunId) {
        this._gunId = gunId;
    },
    getGunId: function () {
        return this._gunId;
    },

    setDamage: function (damage) {
        this._damage = damage;
    },
    getDamage: function () {
        return this._damage;
    },

    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    setFromUserIndex: function (idx) {
        this._userIdx = idx;
    },
    getFromUserIndex: function () {
        return this._userIdx;
    },

    setBulletDirection: function (v) {
        this._bulletDirection = v;
    },
    getBulletDirection: function () {
        return this._bulletDirection;
    },
});

let CharacterDataElement = cc.Node.extend(InjectCCS).extend({
    ctor: function (path) {
        this._super();
        if (!this._ceArr) this.defineCE([
            "sprCharacter",
            "lbName",
            "lbLives",
            "lbAmmo",
            "lbGunName",
            "pnGameOver",
        ]);
        this.injectElement(path || "zccs/game/CharacterDataElement.json");
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },

    presentDataFixed: function () {
        let data = this.getData();
        this.getCE("lbName").setString(data["name"]);
        this.getCE("sprCharacter").setTexture(RoomData.getPathByCharacterType(data["character_type"]));
    },
    presentDataPickUpGun: function () {
        let data = this.getData();
        this.getCE("lbGunName").setString(GunConfig.getGunDataById(data["gun_id"])["name"]);
    },
    presentData: function () {
        let data = this.getData();
        this.getCE("lbLives").setString(data["lives"]);
        this.getCE("lbAmmo").setString(data["ammo"]);
        this.getCE("pnGameOver").setVisible(!!data["death"]);
    },
});

let CharacterDataElementGunGame = CharacterDataElement.extend({
    ctor: function () {
        this.defineCE([
            "sprCharacter",
            "lbName",
            "lbLevel",
            "lbAmmo",
            "lbGunName",
        ]);
        this._super("zccs/game/CharacterDataElementGunGame.json");
    },
    presentData: function () {
        let data = this.getData();
        this.getCE("lbLevel").setString(data["level"] + 1);
        this.getCE("lbAmmo").setString(data["ammo"]);
    },
});

CharacterDataElement.SETTINGS = {
    SIZE: {
        WIDTH: 480
    }
};

let ObjectWithPhysicOnMap = {
    ctor: function () {
        this._super();
    },
    setFalling: function (v) {
        this._falling = v;
    },
    getFalling: function () {
        return this._falling;
    },
    setSpeedX: function (valueX) {
        this._accX = valueX;
    },
    getSpeedX: function () {
        return this._accX;
    },
    setSpeedY: function (valueY) {
        this._accY = valueY;
    },
    getSpeedY: function () {
        return this._accY;
    },
    setLineData: function (v) {
        this._lineData = v;
    },
    getLineData: function () {
        return this._lineData;
    },
    getBoundWidthSpr: function () {
        return this.width;
    },
    getBoundHeightSpr: function () {
        return this.height;
    },
    actionLanding: function () {

    },
};

let CharacterSpriteBattle = cc.Node.extend(ObjectWithPhysicOnMap).extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "sprFire",
            "ndGen",
            "sprWeapon",
            "sprCharacter",
            "sprHit",
            "lbStatus",
            "lbName",
            "prReload",
            "foot1",
            "foot2",
        ]);
        this.injectElement("zccs/game/CharacterSpriteBattle.json");

        this.getCE("sprFire").setOpacity(0);
        this.getCE("sprHit").setOpacity(0);
        this.getCE("sprHit").setPosition(cc.p(0, 0));

        this.getCE("foot1")._originX = this.getCE("foot1").x;
        this.getCE("foot2")._originX = this.getCE("foot2").x;
    },

    runEffectHit: function () {
        let eff = this.getCE("sprHit");
        eff.stopAllActions();
        eff.setOpacity(160);
        eff.runAction(cc.fadeTo(0.2, 0));
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },

    setCharacterDirection: function (v) {
        this._characterDirection = v;
    },
    getCharacterDirection: function () {
        return this._characterDirection;
    },
    setLastFireTimeStamp: function (v) {
        this._lastFireTimeStamp = v;
    },
    getLastFireTimeStamp: function () {
        return this._lastFireTimeStamp
    },
    setLastThrowGrenadeStamp: function (v) {
        this._lastThrowGrenadeStamp = v;
    },
    getLastThrowGrenadeStamp: function () {
        return this._lastThrowGrenadeStamp;
    },

    actionFireBullet: function () {
        let positionData = BattleMgr.GUN_SETTINGS[this._gunId];
        let sprGun = this.getCE("sprWeapon");
        sprGun.x = positionData.x;
        sprGun.y = positionData.y;

        let timeShock = 0.032;
        let timeBack = 0.15;
        let scale = 0.8;

        // scale
        if (sprGun._actionFiringScale) sprGun.stopAction(sprGun._actionFiringScale);
        let actionFiringScale = cc.sequence(
            cc.scaleTo(timeShock, scale, 1),
            cc.scaleTo(timeBack, 1, 1).easing(cc.easeOut(2)),
        );
        sprGun._actionFiringScale = actionFiringScale;
        sprGun.runAction(actionFiringScale);

        // move
        if (sprGun._actionFiringMove) sprGun.stopAction(sprGun._actionFiringMove);
        let distance = (sprGun.width - sprGun.width * scale) / 2;
        let actionFiringMove = cc.sequence(
            cc.moveTo(timeShock, cc.p(positionData.x - distance, positionData.y)),
            cc.moveTo(timeBack, cc.p(positionData.x, positionData.y)).easing(cc.easeOut(2)),
        );
        sprGun._actionFiringMove = actionFiringMove;
        sprGun.runAction(actionFiringMove);

        // rotate
        if (sprGun.__actionFiringRotate) sprGun.stopAction(sprGun.__actionFiringRotate);
        let actionFiringRotate = cc.sequence(
            cc.rotateTo(timeShock, -750 / sprGun.width),
            cc.rotateTo(timeBack, 0).easing(cc.easeOut(2)),
        );
        sprGun.__actionFiringRotate = actionFiringRotate;
        sprGun.runAction(actionFiringRotate);

        // fire
        let sprFire = this.getCE("sprFire");
        sprFire.setOpacity(0);
        let direction = this.getCharacterDirection() === BattleMgr.BULLET_SETTINGS.DIRECTION.TO_LEFT ? -1 : 1;
        sprFire.setScaleX(direction);
        sprFire.x = positionData["start_fire_x"] * direction;
        sprFire.y = positionData["start_fire_y"];
        if (sprFire._actionFire) sprFire.stopAction(sprFire._actionFire);
        let actionFire = cc.sequence(
            cc.fadeIn(0),
            cc.delayTime(timeShock),
            cc.fadeOut(timeBack),
        );
        sprFire._actionFire = actionFire;
        sprFire.runAction(actionFire);
    },
    startRunning: function () {
        if (this._isRunning) return;
        if (this.getFalling()) return;
        this._isRunning = true;

        this.scheduleOnce(() => {
            if (this._isJumping) return;
            this.stepFoot(1);
        }, 0);
        this.scheduleOnce(() => {
            if (this._isJumping) return;
            this.stepFoot(2);
        }, 0.2);
    },
    stepFoot: function (number) {
        let foot = this.getCE("foot" + number);
        foot.stopAllActions();
        foot.x = foot._originX;
        foot.setRotation(0);
        if (this._isRunning) {
            foot.runAction(cc.sequence(
                cc.moveTo(0.3, foot._originX + 12, 0),
                cc.moveTo(0.3, foot._originX, 0),
            ));
            foot.runAction(cc.sequence(
                cc.delayTime(0.2),
                cc.rotateTo(0.1, -30),
                cc.rotateTo(0.1, 0),
                cc.delayTime(0.2),
                cc.callFunc(() => {
                    this.stepFoot(number);
                })
            ));
        }
    },
    stopRunning: function () {
        this._isRunning = false;
    },
    actionJump: function () {
        if (this._isJumping) return;
        this._isJumping = true;
        for (let i = 1; i <= 2; i += 1) {
            let foot = this.getCE("foot" + i);
            foot.stopAllActions();
            foot.x = foot._originX;
            foot.setRotation(0);

            foot.runAction(cc.sequence(
                cc.moveTo(0.1, foot._originX - 4, -4).easing(cc.easeIn(2)),
                cc.delayTime(0.3),
                cc.moveTo(0.1, foot._originX, 0).easing(cc.easeIn(2)),
            ));

            foot.runAction(cc.sequence(
                cc.rotateTo(0.1, 90),
                cc.delayTime(0.3),
                cc.rotateTo(0.1, 0),
            ));
        }
    },

    // override
    actionLanding: function () {
        this._isJumping = false;
        for (let i = 1; i <= 2; i += 1) {
            let foot = this.getCE("foot" + i);
            foot.stopAllActions();
            foot.x = foot._originX;
            foot.y = 0;
            foot.setRotation(0);
        }
    },

    pickUpGun: function (id) {
        let positionData = BattleMgr.GUN_SETTINGS[id];

        let sprGun = this.getCE("sprWeapon");
        sprGun.x = positionData.x;
        sprGun.y = positionData.y;
        sprGun.setTexture(GunConfig.getGunPathById(id));

        let ndGen = this.getCE("ndGen");
        ndGen.x = positionData.start_fire_x;
        ndGen.y = positionData.start_fire_y;

        this._gunId = id;
    },

    // override
    setSpeedX: function (valueX) {
        this._accX = valueX;
        if (this.getLineData() && this._accX !== 0) {
            this.startRunning();
        } else this.stopRunning();
    },
    // override
    getSpeedX: function () {
        return this._accX;
    },
    // override
    getBoundWidthSpr: function () {
        return this.getCE("sprCharacter").width;
    },
    // override
    getBoundHeightSpr: function () {
        return this.getCE("sprCharacter").height;
    },

    setStampReload: function (v) {
        this._stampReload = v;
    },
    getStampReload: function () {
        return this._stampReload;
    },

    setLastHitData: function (v) {
        this._lastHitData = v;
    },
    getLastHitData: function () {
        return this._lastHitData;
    },

    updateStatusString: function (str) {
        let lb = this.getCE("lbStatus");
        lb.setString(str);
    },
    updateReloadStatus: function (percent) {
        if (percent === 100) {
            this.getCE("prReload").setVisible(false);
            this.updateStatusString("");
        } else {
            this.getCE("prReload").setVisible(true);
            this.getCE("prReload").setPercent(percent);
            this.updateStatusString("Reloading");
        }
    },

    resetStat: function () {
        this.setFalling(true);
        this.setSpeedX(0);
        this.setSpeedY(0);
        this.setLineData(null);
        this.setLastFireTimeStamp(0);
        this.setLastThrowGrenadeStamp(0);
        this.setStampReload(0);
        this.updateReloadStatus(100);
    },

    presentTeam: function () {
        let data = this.getData();
        switch (data["team"]) {
            case RoomConst.TEAM.A:
                this.getCE("lbName").setTextColor(cc.color("#FF0000"));
                break;
            case RoomConst.TEAM.B:
                this.getCE("lbName").setTextColor(cc.color("#009AFF"));
                break;
        }

    },
    presentData: function () {
        let data = this.getData();
        this.getCE("lbName").setString(data["name"]);
        this.getCE("sprCharacter").setTexture(RoomData.getPathByCharacterType(data["character_type"]));
        let colorStr;
        switch (data["character_type"]) {
            case RoomConst.CHARACTER_TYPE.BLUE:
                colorStr = "#0013FF";
                break;
            case RoomConst.CHARACTER_TYPE.CYAN:
                colorStr = "#007661";
                break;
            case RoomConst.CHARACTER_TYPE.PINK:
                colorStr = "#FF19E4";
                break;
            case RoomConst.CHARACTER_TYPE.RED:
                colorStr = "#FF0000";
                break;
        }

        this.getCE("foot1").setColor(cc.color(colorStr));
        this.getCE("foot2").setColor(cc.color(colorStr));
    },
});

let SupplyBox = cc.Sprite.extend({
    ctor: function () {
        this._super("sprites/map/item_gun_box.png");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    setGunId: function (v) {
        this._gunId = v;
    },
    getGunId: function () {
        return this._gunId
    }
});

let DustEff = cc.Sprite.extend({
    ctor: function () {
        this._super("sprites/effect/dust.png");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
});

let BloodHitEff = cc.Sprite.extend({
    ctor: function () {
        this._super("sprites/effect/blood_2.png");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
});

let BloodFlyEff = cc.Sprite.extend({
    ctor: function () {
        this._super("sprites/effect/blood.png");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
});

let NodeTextKill = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "lbKiller",
            "sprWeapon",
            "lbDeath",
            "sprSelfDestruct"
        ]);
        this.injectElement("zccs/game/NodeTextKill.json");
        this.getCE("sprSelfDestruct").setVisible(false);
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    presentKillData: function (sprDeath, sprKiller, matchMode) {
        let lbKiller = this.getCE("lbKiller");
        let sprWeapon = this.getCE("sprWeapon");
        let lbDeath = this.getCE("lbDeath");
        let sprSelfDestruct = this.getCE("sprSelfDestruct");
        let isSelfDestruct = !sprKiller;

        let killerData = sprKiller ? sprKiller.getData() : {};
        let deathData = sprDeath.getData();

        let getTextWidth = function (text) {
            return StringUtils.getTextSize(text, lbKiller.getFontName(), lbKiller.getFontSize()).width
        };
        let strKiller = isSelfDestruct ? "" : killerData["name"];
        lbKiller.setString(strKiller);
        let killerWidth = getTextWidth(strKiller);

        let strDeath = deathData["name"];
        lbDeath.setString(strDeath);
        let deathWidth = getTextWidth(strDeath);

        let range;
        if (isSelfDestruct) {
            sprSelfDestruct.setVisible(true);
            sprWeapon.setVisible(false);

            let space = 24;
            lbDeath.x = space + sprSelfDestruct.width + space;
            range = space + sprSelfDestruct.width + space + deathWidth + space;
        } else {
            sprSelfDestruct.setVisible(false);
            sprWeapon.setVisible(true);
            let lastHitData = sprDeath.getLastHitData();
            let weaponId = lastHitData.weaponId;
            switch (lastHitData.damageDealerType) {
                case DAMAGE_DEALER_TYPE.GUN:
                    sprWeapon.setTexture(GunConfig.getGunPathById(weaponId));
                    break;
                case DAMAGE_DEALER_TYPE.GRENADE:
                    sprWeapon.setTexture(GrenadeConfig.getGrenadePathById(weaponId));
                    break;
            }

            let space = 24;
            lbKiller.x = space;
            sprWeapon.x = space + killerWidth + space + sprWeapon.width / 2;
            lbDeath.x = space + killerWidth + space + sprWeapon.width + space;
            range = space + killerWidth + sprWeapon.width + space + deathWidth + space;
        }

        if (matchMode === RoomConst.ROOM_MODE.LAST_TEAM_STANDING) {
            if (!isSelfDestruct) {
                switch (killerData["team"]) {
                    case RoomConst.TEAM.A:
                        lbKiller.setTextColor(cc.color("#FF0000"));
                        break;
                    case RoomConst.TEAM.B:
                        lbKiller.setTextColor(cc.color("#009AFF"));
                        break;
                }
            }

            switch (deathData["team"]) {
                case RoomConst.TEAM.A:
                    lbDeath.setTextColor(cc.color("#FF0000"));
                    break;
                case RoomConst.TEAM.B:
                    lbDeath.setTextColor(cc.color("#009AFF"));
                    break;
            }
        }

        return range;
    }
});

let SpriteGrenade = cc.Sprite.extend(ObjectWithPhysicOnMap).extend({
    ctor: function (grenadeId) {
        this._super();
        let path = GrenadeConfig.getGrenadePathById(grenadeId);
        this.anchorY = 0;
        this.setTexture(path);
        this.setId(grenadeId);
    },
    setId: function (v) {
        this._id = v;
    },
    getId: function () {
        return this._id;
    },

    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },

    setDirection: function (v) {
        this._direction = v;
    },
    setThrownFrom: function (v) {
        this._fromUserIdx = v;
    },
    getThrownFrom: function () {
        return this._fromUserIdx;
    },
    setStampThrown: function (v) {
        this._stampThrown = v;
    },
    getStampThrown: function () {
        return this._stampThrown;
    },

    setSmoke: function (v) {
        this._smoke = v;
    },
    getSmoke: function () {
        return this._smoke;
    }
});

let NodeExplode = cc.Node.extend(InjectCCS).extend({

    ctor: function () {
        this._super();
        this.defineCE([
            "sprCircle2",
            "sprExplode",
            "sprCircle",
            "sprExplode2",
            "lbText"
        ]);
        this.injectElement("zccs/game/NodeExplode.json");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    runActionExplode: function (grenade) {
        let dust = this;
        // let rangeRadius = GrenadeConfig.getGrenadeDataById(grenade.getId())["range_radius"];
        // let targetScale = (rangeRadius * 2) / dust.width;
        dust.x = grenade.x;
        dust.y = grenade.y;

        let sprExplode = this.getCE("sprExplode");
        sprExplode.setOpacity(255);
        sprExplode.setScale(0.7);
        sprExplode.runAction(cc.sequence(
            cc.scaleTo(0.1, 1).easing(cc.easeOut(2)),
            cc.fadeOut(0.1),
        ));

        let sprCircle2 = this.getCE("sprCircle2");
        sprCircle2.setOpacity(255);
        sprCircle2.setScale(0.3);
        sprCircle2.runAction(cc.sequence(
            cc.scaleTo(0.1, 1).easing(cc.easeOut(2)),
            cc.fadeOut(0.3),
        ));

        let sprCircle = this.getCE("sprCircle");
        sprCircle.setOpacity(255);
        sprCircle.setScale(0.3);
        sprCircle.runAction(cc.sequence(
            cc.scaleTo(0.1, 1).easing(cc.easeOut(2)),
            cc.fadeOut(0.3),
        ));

        let lbText = this.getCE("lbText");
        lbText.setOpacity(255);
        lbText.setScale(0.3);
        lbText.runAction(cc.sequence(
            cc.scaleTo(0.1, 1).easing(cc.easeOut(2)),
            cc.fadeOut(0.3),
            cc.callFunc(() => {
                this.setActive(false)
            })
        ));

        let sprExplode2 = this.getCE("sprExplode2");
        sprExplode2.setOpacity(40);
        sprExplode2.runAction(cc.fadeTo(0.3, 0));
    },
});

let NodeFlash = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "sprExplode2",
            "sprExplode"
        ]);
        this.injectElement("zccs/game/NodeFlash.json");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    runActionFlash: function (grenade) {
        let dust = this;
        dust.x = grenade.x;
        dust.y = grenade.y;
        let rangeRadius = GrenadeConfig.getGrenadeDataById(grenade.getId())["range_radius"];
        let width = 200;
        let targetScale = (rangeRadius * 2) / width;

        let sprExplode = this.getCE("sprExplode");
        sprExplode.setOpacity(255);
        sprExplode.setScale(0.7);
        sprExplode.runAction(cc.sequence(
            cc.scaleTo(0.1, targetScale).easing(cc.easeOut(2)),
            cc.fadeOut(0.1),
        ));

        let sprExplode2 = this.getCE("sprExplode2");
        sprExplode2.setOpacity(40);
        sprExplode2.runAction(cc.sequence(
            cc.fadeTo(0.3, 0),
            cc.callFunc(() => {
                this.setActive(false)
            })
        ));
    },
});

let NodeSmoke = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        let arr = [];
        for (let i = 0; i <= 5; i += 1) arr.push("sprSmoke" + i);
        this.defineCE(arr);
        this.injectElement("zccs/game/NodeSmoke.json");
    },
    setActive: function (v) {
        this._active = v;
    },
    getActive: function () {
        return this._active;
    },
    runActionShowSmokeOnScene: function (grenade) {
        this.x = grenade.x;
        this.y = grenade.y;

        let dataConf = GrenadeConfig.getGrenadeDataById(grenade.getId());
        let rangeRadius = dataConf["range_radius"];
        let width = 200;
        let targetScale = rangeRadius * 2 / width;

        for (let i = 0; i <= 5; i += 1) {
            let smoke = this.getCE("sprSmoke" + i);
            let durationAction = 2 + Math.random();
            smoke.setOpacity(0);
            smoke.setRotation(Math.random() * 360);

            smoke.runAction(cc.sequence(
                cc.fadeTo(durationAction, 100 + Math.random() * 50),
                cc.fadeTo(durationAction, 200),
            ).repeatForever());

            smoke.runAction(cc.sequence(
                cc.scaleTo(durationAction, (0.5 + Math.random() * 0.5) * targetScale),
                cc.scaleTo(durationAction, targetScale),
            ).repeatForever());
        }
    },
    stopSmokeOnScene: function () {
        for (let i = 0; i <= 5; i += 1) {
            let smoke = this.getCE("sprSmoke" + i);
            smoke.stopAllActions();
            smoke.runAction(cc.fadeOut(Math.random() * 3));
        }
        this.runAction(cc.sequence(
            cc.delayTime(3),
            cc.callFunc(() => {
                this.setActive(false);
            })
        ))
    },
});