let GuiRoom = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnExit",
            "btnContinue",
            "nd"
        ]);
        this.injectElement("zccs/room/GuiRoom.json");
        this.initUserUtils();
    },
    initUserUtils: function () {
        let nd = this.getCE("nd");
        this._userElementArr = [];
        let posX = 0;
        let width = CellUserItem.SETTINGS.SIZE.WIDTH;

        let storageStr = localStorage.getItem("playerRoomDataArr");
        let dataArr = [];
        if (storageStr) {
            dataArr = JSON.parse(storageStr);
        } else {
            let player0 = {
                "name": "Player 1",
                "character_type": RoomConst.CHARACTER_TYPE.BLUE,
                "gun": GunConst.TYPE.DESERT_EAGLE,
                "grenade": GrenadeConst.ID.EXPLOSIVE_GRENADE,
                "team": RoomConst.TEAM.A,
                "active": RoomConst.ACTIVE_TYPE.ACTIVE
            };

            let player1 = {
                "name": "Player 2",
                "character_type": RoomConst.CHARACTER_TYPE.CYAN,
                "gun": GunConst.TYPE.DESERT_EAGLE,
                "grenade": GrenadeConst.ID.EXPLOSIVE_GRENADE,
                "team": RoomConst.TEAM.A,
                "active": RoomConst.ACTIVE_TYPE.ACTIVE
            };

            let player2 = {
                "name": "Player 3",
                "character_type": RoomConst.CHARACTER_TYPE.RED,
                "gun": GunConst.TYPE.DESERT_EAGLE,
                "grenade": GrenadeConst.ID.EXPLOSIVE_GRENADE,
                "team": RoomConst.TEAM.B,
                "active": RoomConst.ACTIVE_TYPE.NONE
            };

            let player3 = {
                "name": "Player 4",
                "character_type": RoomConst.CHARACTER_TYPE.PINK,
                "gun": GunConst.TYPE.DESERT_EAGLE,
                "grenade": GrenadeConst.ID.EXPLOSIVE_GRENADE,
                "team": RoomConst.TEAM.B,
                "active": RoomConst.ACTIVE_TYPE.NONE
            };

            dataArr = [player0, player1, player2, player3];
            localStorage.setItem("playerRoomDataArr", JSON.stringify(dataArr));
        }

        RoomData.setUserArr(dataArr);

        for (let i = 0; i < 4; i += 1) {
            let userElement = new CellUserItem;
            userElement.x = posX;
            posX += width;

            nd.addChild(userElement);
            userElement.setIndex(i);
            userElement.setData(dataArr[i]);
            userElement.presentData();
            this._userElementArr.push(userElement);
        }

        this.updateButtonDoneUserSettings();
    },
    scrollViewPresentDataAtIndex: function (idx) {
        this._userElementArr[idx].presentData();
    },
    enableConitnue: function (v) {
        if (v) {
            this.getCE("btnContinue").setColor(cc.color("#dbd500"));
            this.getCE("btnContinue").setEnabled(true);
        } else {
            this.getCE("btnContinue").setColor(cc.color("#7f7f7f"));
            this.getCE("btnContinue").setEnabled(false);
        }
    },
    updateButtonDoneUserSettings: function () {
        if (RoomData.getRoomMode() === RoomConst.ROOM_MODE.LAST_TEAM_STANDING) {
            let haveTeamA = !!RoomData.getUserArr().some((item) => {
                return item["team"] === RoomConst.TEAM.A && item["active"] === RoomConst.ACTIVE_TYPE.ACTIVE;
            });
            let haveTeamB = !!RoomData.getUserArr().some((item) => {
                return item["team"] === RoomConst.TEAM.B && item["active"] === RoomConst.ACTIVE_TYPE.ACTIVE;
            });
            if (!haveTeamA || !haveTeamB) {
                this.enableConitnue(false);
                return;
            }
        }

        let arr = RoomData.getUserArr().filter((item) => {
            return item["active"] === RoomConst.ACTIVE_TYPE.ACTIVE;
        });
        if (arr.length < 2) {
            this.enableConitnue(false);
            return;
        }

        this.enableConitnue(true);
    },
    onTouchEndedEvent: function (sender) {
        // this._userElementArr.forEach((item, index) => {
        //     RoomData.updateUserNameAtIndex(item.getCE("tfNameInput").getString(), index);
        // });

        switch (sender) {
            case this.getCE("btnExit"):
                GuiMgr.removeGUI(GuiRoom);
                break;
            case this.getCE("btnContinue"):
                RoomMgr.doneUserSettings();
                break;
        }
    }
});

let CellUserItem = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "sprCharacter",
            "btnRemove",
            "btnAddUser",
            "pnCoverer",

            "ndGunUtils",
            "btnGun",
            "sprGun",

            "btnColor",
            "pnColor",

            "ndGrenadeUtils",
            "btnGrenade",
            "sprGrenade",

            "tfNameInput",
            "ndTeamUtils",
            "btnTeamA",
            "btnTeamB"
        ]);
        this.injectElement("zccs/room/CellUserRoom.json");
        this.initTf();
    },
    initTf: function () {
        this.getCE("tfNameInput").addEventListener(() => {
            RoomData.updateUserNameAtIndex(this.getCE("tfNameInput").getString(), this.getIndex());
        });
    },
    setIndex: function (index) {
        this._index = index;
    },
    getIndex: function () {
        return this._index;
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },

    presentTeamData: function (team) {
        switch (team) {
            case RoomConst.TEAM.A:
                this.getCE("btnTeamA").setColor(cc.color("#FFFF00"));
                this.getCE("btnTeamB").setColor(cc.color("#FFFFFF"));
                break;
            case RoomConst.TEAM.B:
                this.getCE("btnTeamB").setColor(cc.color("#FFFF00"));
                this.getCE("btnTeamA").setColor(cc.color("#FFFFFF"));
                break;
        }
    },
    presentActiveData: function () {
        let data = this.getData();
        let cover = this.getCE("pnCoverer");
        cover.stopAllActions();
        let posY;
        if (data["active"] === RoomConst.ACTIVE_TYPE.NONE) posY = CellUserItem.SETTINGS.POSITION_COVER.USER_NONE;
        else posY = CellUserItem.SETTINGS.POSITION_COVER.USER_ACTIVE;
        cover.runAction(cc.moveTo(0.3, cover.x, posY).easing(cc.easeIn(2.4)));
    },
    presentData: function () {
        let data = this.getData();

        this.getCE("ndTeamUtils").setVisible(RoomData.getRoomMode() === RoomConst.ROOM_MODE.LAST_TEAM_STANDING);
        this.presentTeamData(data["team"]);
        this.getCE("tfNameInput").setString(data["name"]);

        this.getCE("sprGun").setTexture(GunConfig.getGunPathById(data["gun"]));
        this.getCE("ndGunUtils").setVisible(RoomData.getRoomMode() !== RoomConst.ROOM_MODE.GUN_GAME);

        this.getCE("sprGrenade").setTexture(GrenadeConfig.getGrenadePathById(data["grenade"]));
        this.getCE("ndGrenadeUtils").setVisible(RoomData.getRoomMode() !== RoomConst.ROOM_MODE.GUN_GAME);

        this.getCE("sprCharacter").setTexture(RoomData.getPathByCharacterType(data["character_type"]));
        this.getCE("pnColor").setColor(RoomData.getColorByCharacterType(data["character_type"]));

        this.presentActiveData();
    },

    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnGun"):
                var gui = GuiMgr.viewGUI(GuiGunSelect);
                gui.selectGun(this.getData()["gun"]);
                gui.setCallbackDone(() => {
                    RoomData.updateGunAtIndex(gui.getSelectedGunId(), this.getIndex());
                    GuiMgr.getGUI(GuiRoom).scrollViewPresentDataAtIndex(this.getIndex());
                    GuiMgr.removeGUI(GuiGunSelect);
                });
                break;
            case this.getCE("btnColor"):
                var gui = GuiMgr.viewGUI(GuiCharacterSelect);
                gui.selectCharacter(this.getData()["character_type"]);
                gui.setCallbackDone(() => {
                    RoomData.updateCharacterAtIndex(gui.getSelectedCharacterType(), this.getIndex());
                    GuiMgr.getGUI(GuiRoom).scrollViewPresentDataAtIndex(this.getIndex());
                    GuiMgr.removeGUI(GuiCharacterSelect);
                });
                break;
            case this.getCE("btnGrenade"):
                var gui = GuiMgr.viewGUI(GuiGrenadeSelect);
                gui.selectNade(this.getData()["grenade"]);
                gui.setCallbackDone(() => {
                    RoomData.updateGrenadeAtIndex(gui.getSelectedNadeId(), this.getIndex());
                    GuiMgr.getGUI(GuiRoom).scrollViewPresentDataAtIndex(this.getIndex());
                    GuiMgr.removeGUI(GuiGrenadeSelect);
                });
                break;
            case this.getCE("btnTeamA"):
                RoomData.updateTeamAtIndex(RoomConst.TEAM.A, this.getIndex());
                this.presentTeamData(RoomConst.TEAM.A);
                GuiMgr.getGUI(GuiRoom).updateButtonDoneUserSettings();
                break;
            case this.getCE("btnTeamB"):
                RoomData.updateTeamAtIndex(RoomConst.TEAM.B, this.getIndex());
                this.presentTeamData(RoomConst.TEAM.B);
                GuiMgr.getGUI(GuiRoom).updateButtonDoneUserSettings();
                break;
            case this.getCE("btnAddUser"):
                RoomData.updateVisibleAtIndex(RoomConst.ACTIVE_TYPE.ACTIVE, this.getIndex());
                this.presentActiveData();
                GuiMgr.getGUI(GuiRoom).updateButtonDoneUserSettings();
                break;
            case this.getCE("btnRemove"):
                RoomData.updateVisibleAtIndex(RoomConst.ACTIVE_TYPE.NONE, this.getIndex());
                this.presentActiveData();
                GuiMgr.getGUI(GuiRoom).updateButtonDoneUserSettings();
                break;
        }
    }
});

CellUserItem.SETTINGS = {
    SIZE: {
        WIDTH: 480,
        HEIGHT: 1080
    },
    POSITION_COVER: {
        USER_ACTIVE: 888,
        USER_NONE: 0
    }
};
