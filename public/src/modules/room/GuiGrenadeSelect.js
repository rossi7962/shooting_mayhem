let GuiGrenadeSelect = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnSelect",
            "svGrenadeList",
            "lbGrenadeName",
            "lbDescription"
        ]);
        this.injectElement("zccs/room/GuiGrenadeSelect.json");
        this.initScrollView();
    },
    initScrollView: function () {
        let self = this;
        let grenadeArr = GrenadeConfig.getGrenadeArray();

        let svGrenadeList = this.getCE("svGrenadeList");
        let width = CellGrenadeElement.SETTINGS.SIZE.WIDTH * grenadeArr.length;
        width = width < svGrenadeList.width ? svGrenadeList.width : width;
        svGrenadeList.setInnerContainerSize(cc.size(
            width,
            CellGrenadeElement.SETTINGS.SIZE.HEIGHT
        ));
        this._grenadeElementArray = [];
        let posX = 0;

        grenadeArr.forEach(function (item) {
            let grenadeItem = new CellGrenadeElement();
            grenadeItem.setData(item);
            grenadeItem.presentData();

            grenadeItem.x = posX;
            posX += CellGrenadeElement.SETTINGS.SIZE.WIDTH;

            self.getCE("svGrenadeList").addChild(grenadeItem);
            self._grenadeElementArray.push(grenadeItem);
        });

        this._selectedNade = -1;
    },
    selectNade: function (id) {
        this._grenadeElementArray.forEach((element) => {
            element.deselect();
        });

        let grenadeElement = this._grenadeElementArray.find((element) => {
            return element.getData()["id"] === id
        });
        grenadeElement.select();

        this._selectedNade = id;
        let data = GrenadeConfig.getGrenadeDataById(id);
        this.getCE("lbDescription").setString(data["description"]);
        this.getCE("lbGrenadeName").setString(data["name"]);
    },
    getSelectedNadeId: function () {
        return this._selectedNade;
    },
    setCallbackDone: function (v) {
        this._callBackDone = v;
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnSelect"):
                this._callBackDone();
                break;
        }
    }
});

let CellGrenadeElement = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "pnSelected",
            "btnNade",
            "sprNade"
        ]);
        this.injectElement("zccs/room/CellGrenadeElement.json");
        this.getCE("btnNade").setSwallowTouches(false);
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },
    select: function () {
        this.getCE("pnSelected").setVisible(true);
    },
    deselect: function () {
        this.getCE("pnSelected").setVisible(false);
    },
    presentData: function () {
        var data = this.getData();
        this.getCE("sprNade").setTexture(GrenadeConfig.getGrenadePathById(data["id"]));
    },
    onTouchMovedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnNade"):
                let starter = sender.getTouchBeganPosition();
                let pos = sender.getTouchMovePosition();
                if (Math.abs(starter.y - pos.y) > 20) this._moving = true;
                break;
        }
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnNade"):
                if (this._moving) {
                    this._moving = false;
                    return;
                } else {
                    this.select();
                    GuiMgr.getGUI(GuiGrenadeSelect).selectNade(this.getData()["id"]);
                }
                break;
        }
    }
});

CellGrenadeElement.SETTINGS = {
    SIZE: {
        WIDTH: 200,
        HEIGHT: 200
    }
};
