let GuiModeSelector = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "lbTitle",
            "btnLastManStanding",
            "btnGunGame",
            "btnLastTeamStanding",
            "btnExit",
            "btnContinue"
        ]);
        this.injectElement("zccs/room/GuiModeSelector.json");
        this.init();
    },

    init: function () {
        this.getCE("lbTitle").setString(GAME_TITLE);

        this.handleClickMode(this.getCE("btnLastManStanding"));
        this.setMode(RoomConst.ROOM_MODE.LAST_MAN_STANDING);

        var list = [
            "btnLastManStanding",
            "btnGunGame",
            "btnLastTeamStanding",
        ];

        list.forEach((name) => {
            this.getCE(name).setPressedActionEnabled(false);
        });
    },
    setMode: function (v) {
        this._mode = v;
    },
    getMode: function () {
        return this._mode;
    },

    handleClickMode: function (sender) {
        var list = [
            "btnLastManStanding",
            "btnGunGame",
            "btnLastTeamStanding",
        ];

        list.forEach((name) => {
            this.getCE(name).setColor(cc.color("#ffffff"));
        });

        sender.setColor(cc.color("#DBD500"));
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnLastManStanding"):
                this.setMode(RoomConst.ROOM_MODE.LAST_MAN_STANDING);
                this.handleClickMode(sender);
                break;
            case this.getCE("btnGunGame"):
                this.setMode(RoomConst.ROOM_MODE.GUN_GAME);
                this.handleClickMode(sender);
                break;
            case this.getCE("btnLastTeamStanding"):
                this.setMode(RoomConst.ROOM_MODE.LAST_TEAM_STANDING);
                this.handleClickMode(sender);
                break;
            case this.getCE("btnContinue"):
                RoomMgr.doneSelectMode(this.getMode());
                break;
            case this.getCE("btnExit"):
                GuiMgr.removeGUI(GuiModeSelector);
                break;
        }
    }
});
