let GuiMapSelector = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "lbTitle",
            "btnExit",
            "btnRandom",
            "btnContinue",
            "pnMapReview",
            "svMapList"
        ]);
        this.injectElement("zccs/room/GuiMapSelector.json");
        this.initGUI();
    },
    initGUI: function () {
        this.getCE("lbTitle").setString(GAME_TITLE);

        let self = this;
        let mapArr = MapConfig.getMapArray();

        let svMapList = this.getCE("svMapList");
        let height = MapItem.SETTINGS.SIZE.HEIGHT * mapArr.length;
        height = height < svMapList.height ? svMapList.height : height;
        svMapList.setInnerContainerSize(cc.size(
            MapItem.SETTINGS.SIZE.WIDTH,
            height
        ));
        this._mapArrElement = [];
        let posY = height;

        mapArr.forEach(function (item) {
            let mapItem = new MapItem();
            mapItem.setData(item);
            mapItem.presentData();
            posY -= MapItem.SETTINGS.SIZE.HEIGHT;
            mapItem.y = posY;
            self.getCE("svMapList").addChild(mapItem);
            self._mapArrElement.push(mapItem);
        });

        this._selectedMapId = -1;
    },
    onSelectMap: function (itemElement) {
        var data = itemElement.getData();
        this._mapArrElement.forEach((item) => {
            item.cellDeselected();
        });
        itemElement.cellSelected();
        this._selectedMapId = data["id"];

        if (this._previewMapSpr) {
            this._previewMapSpr.setVisible(true);
        } else {
            this._previewMapSpr = new cc.Sprite();
            let previewPanel = this.getCE("pnMapReview");

            this._previewMapSpr.x = previewPanel.width / 2;
            this._previewMapSpr.anchorY = 0.65;
            this._previewMapSpr.y = 326;

            previewPanel.addChild(this._previewMapSpr);
        }

        var path = MapConfig.getMapPathById(data["id"]);
        this._previewMapSpr.setTexture(path);
    },
    onRandomMap: function () {
        this._mapArrElement.forEach((item) => {
            item.cellDeselected();
        });
        this._selectedMapId = -1;

        if (this._previewMapSpr) this._previewMapSpr.setVisible(false);
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnExit"):
                GuiMgr.removeGUI(GuiMapSelector);
                break;
            case this.getCE("btnRandom"):
                this.onRandomMap();
                break;
            case this.getCE("btnContinue"):
                RoomMgr.doneSelectMap(this._selectedMapId);
                break;
        }
    }
});

let MapItem = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnSetMap",
            "lbMapNameActive",
            "lbMapName"
        ]);
        this.injectElement("zccs/room/CellMap.json");
        this.cellDeselected();
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },
    cellSelected: function () {
        this.getCE("lbMapName").setVisible(false);
        this.getCE("lbMapNameActive").setVisible(true);
    },
    cellDeselected: function () {
        this.getCE("lbMapName").setVisible(true);
        this.getCE("lbMapNameActive").setVisible(false);
    },
    presentData: function () {
        var data = this.getData();
        this.getCE("lbMapName").setString(data["name"]);
        this.getCE("lbMapNameActive").setString(data["name"]);
    },
    onTouchMovedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnSetMap"):
                let starter = sender.getTouchBeganPosition();
                let pos = sender.getTouchMovePosition();
                if (Math.abs(starter.y - pos.y) > 20) this._moving = true;
                break;
        }
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnSetMap"):
                if (this._moving) {
                    this._moving = false;
                    return;
                } else {
                    let guiMapSelect = GuiMgr.getGUI(GuiMapSelector);
                    if (guiMapSelect) guiMapSelect.onSelectMap(this);
                }
                break;
        }
    }
});

MapItem.SETTINGS = {
    SIZE: {
        WIDTH: 600,
        HEIGHT: 100
    }
};
