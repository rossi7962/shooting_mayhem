let GuiGunSelect = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnSelect",
            "svGunList",
            "lbGunName",

            "prDamage",
            "prWeight",
            "prRecoil",
            "prAttackDelay",
            "prAmmo",
            "prReloadTime"
        ]);
        this.injectElement("zccs/room/GuiGunSelect.json");
        this.initScrollView();
    },
    initScrollView: function () {
        let self = this;
        let gunArr = GunConfig.getGunArray();
        gunArr = gunArr.filter((item) => {
            return item["type"] === GunConst.TIER.HAND_GUN;
        });

        let svMapList = this.getCE("svGunList");
        let width = CellGunElement.SETTINGS.SIZE.WIDTH * gunArr.length;
        width = width < svMapList.width ? svMapList.width : width;
        svMapList.setInnerContainerSize(cc.size(
            width,
            CellGunElement.SETTINGS.SIZE.HEIGHT
        ));
        this._gunArrayElement = [];
        let posX = 0;

        gunArr.forEach(function (item) {
            let gunItem = new CellGunElement();
            gunItem.setData(item);
            gunItem.presentData();

            gunItem.x = posX;
            posX += CellGunElement.SETTINGS.SIZE.WIDTH;

            self.getCE("svGunList").addChild(gunItem);
            self._gunArrayElement.push(gunItem);
        });

        this._selectedGunId = -1;
    },
    selectGun: function (id) {
        let data = GunConfig.getGunDataById(id);
        this._gunArrayElement.forEach((element) => {
            element.deselect();
        });

        let gunElement = this._gunArrayElement.find((element) => {
            return element.getData()["id"] === id
        });
        gunElement.select();

        let damageHighest = GunConfig.getGunHaveHighestStatField("damage")["damage"];
        this.getCE("prDamage").setPercent(data["damage"]/damageHighest * 100);

        let weightHighest = GunConfig.getGunHaveHighestStatField("weight")["weight"];
        this.getCE("prWeight").setPercent(data["weight"]/weightHighest * 100);

        let recoilHighest = GunConfig.getGunHaveHighestStatField("recoil")["recoil"];
        this.getCE("prRecoil").setPercent(data["recoil"]/recoilHighest * 100);

        let attackDelayHighest = GunConfig.getGunHaveHighestStatField("attack_delay")["attack_delay"];
        this.getCE("prAttackDelay").setPercent(data["attack_delay"]/attackDelayHighest * 100);

        let ammoHighest = GunConfig.getGunHaveHighestStatField("ammo")["ammo"];
        this.getCE("prAmmo").setPercent(data["ammo"]/ammoHighest * 100);

        let reloadTimeHighest = GunConfig.getGunHaveHighestStatField("reload_time")["reload_time"];
        this.getCE("prReloadTime").setPercent(data["reload_time"]/reloadTimeHighest * 100);

        this._selectedGunId = id;
        this.getCE("lbGunName").setString(data["name"]);
    },
    getSelectedGunId: function () {
        return this._selectedGunId;
    },
    setCallbackDone: function (v) {
        this._callBackDone = v;
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnSelect"):
                this._callBackDone();
                break;
        }
    }
});

let CellGunElement = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "pnSelected",
            "btnGun",
            "sprGun"
        ]);
        this.injectElement("zccs/room/CellGunElement.json");
        this.getCE("btnGun").setSwallowTouches(false);
    },
    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },
    select: function () {
        this.getCE("pnSelected").setVisible(true);
    },
    deselect: function () {
        this.getCE("pnSelected").setVisible(false);
    },
    presentData: function () {
        var data = this.getData();
        this.getCE("sprGun").setTexture(GunConfig.getGunPathById(data["id"]));
    },
    onTouchMovedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnGun"):
                let starter = sender.getTouchBeganPosition();
                let pos = sender.getTouchMovePosition();
                if (Math.abs(starter.y - pos.y) > 20) this._moving = true;
                break;
        }
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnGun"):
                if (this._moving) {
                    this._moving = false;
                    return;
                } else {
                    this.select();
                    GuiMgr.getGUI(GuiGunSelect).selectGun(this.getData()["id"]);
                }
                break;
        }
    }
});

CellGunElement.SETTINGS = {
    SIZE: {
        WIDTH: 200,
        HEIGHT: 200
    }
};
