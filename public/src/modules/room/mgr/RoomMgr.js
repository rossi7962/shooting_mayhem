let RoomData = {
    setRoomMode: function (v) {
        this._roomMode = v;
    },
    getRoomMode: function () {
        return this._roomMode;
    },

    setMapId: function (id) {
        this._mapId = id;
    },
    getMapId: function () {
        return this._mapId;
    },

    setUserArr: function (v) {
        this._userArr = v;
    },
    getUserArr: function () {
        return this._userArr;
    },

    updateGunAtIndex: function (gunId, userIndex) {
        this.getUserArr()[userIndex]["gun"] = gunId;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },
    updateCharacterAtIndex: function (characterType, userIndex) {
        this.getUserArr()[userIndex]["character_type"] = characterType;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },
    updateTeamAtIndex: function (team, userIndex) {
        this.getUserArr()[userIndex]["team"] = team;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },
    updateGrenadeAtIndex: function (grenade, userIndex) {
        this.getUserArr()[userIndex]["grenade"] = grenade;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },
    updateUserNameAtIndex: function (name, userIndex) {
        this.getUserArr()[userIndex]["name"] = name;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },
    updateVisibleAtIndex: function (visible, userIndex) {
        this.getUserArr()[userIndex]["active"] = visible;
        localStorage.setItem("playerRoomDataArr", JSON.stringify(this.getUserArr()));
    },

    getPathByCharacterType: function (type) {
        switch (type) {
            case RoomConst.CHARACTER_TYPE.BLUE: return "sprites/character/blue_shooter.png";
            case RoomConst.CHARACTER_TYPE.CYAN: return "sprites/character/cyan_shooter.png";
            case RoomConst.CHARACTER_TYPE.PINK: return "sprites/character/pinky_shooter.png";
            case RoomConst.CHARACTER_TYPE.RED: return "sprites/character/red_shooter.png";
        }
    },
    getColorByCharacterType: function (type) {
        switch (type) {
            case RoomConst.CHARACTER_TYPE.BLUE: return cc.color("#009AFF");
            case RoomConst.CHARACTER_TYPE.CYAN: return cc.color("#26ECC9");
            case RoomConst.CHARACTER_TYPE.PINK: return cc.color("#FF8FF2");
            case RoomConst.CHARACTER_TYPE.RED: return cc.color("#FF7C7C");
        }
    }
};

let RoomMgr = {
    doneSelectMode: function (mode) {
        RoomData.setRoomMode(mode);
        GuiMgr.viewGUI(GuiMapSelector);
    },
    doneSelectMap: function (mapId) {
        RoomData.setMapId(mapId);
        if (mapId === -1) {
            let mapArr = MapConfig.getMapArray();
            let map = mapArr[Math.floor(Math.random() * Math.floor(mapArr.length))];
            RoomData.setMapId(map["id"]);
        }
        GuiMgr.viewGUI(GuiRoom);
    },
    doneUserSettings: function () {
        GuiMgr.viewGUI(SceneBattle);
        GuiMgr.removeGUI(GuiRoom);
        GuiMgr.removeGUI(GuiMapSelector);
        GuiMgr.removeGUI(GuiModeSelector);
    }
};

let RoomConst = {
    ROOM_MODE: {
        LAST_MAN_STANDING: 0,
        LAST_TEAM_STANDING: 1,
        SUDDEN_DEATH: 2,
        GUN_GAME: 3,
    },
    CHARACTER_TYPE: {
        BLUE: 0,
        CYAN: 1,
        PINK: 2,
        RED: 3
    },
    ACTIVE_TYPE: {
        NONE: 0,
        ACTIVE: 1
    },
    TEAM: {
        A: 0,
        B: 1
    }
};
