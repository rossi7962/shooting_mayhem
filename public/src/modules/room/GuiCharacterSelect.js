let GuiCharacterSelect = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "pnSelection",
            "btnSelect",
            "btnBlue",
            "btnCyan",
            "btnPink",
            "btnRed"
        ]);
        this.injectElement("zccs/room/GuiCharacterSelect.json");
    },

    getBtnByCharacterType: function (type) {
        switch (type) {
            case RoomConst.CHARACTER_TYPE.BLUE:
                return this.getCE("btnBlue");
            case RoomConst.CHARACTER_TYPE.CYAN:
                return this.getCE("btnCyan");
            case RoomConst.CHARACTER_TYPE.PINK:
                return this.getCE("btnPink");
            case RoomConst.CHARACTER_TYPE.RED:
                return this.getCE("btnRed");
        }
    },
    getCharacterTypeByButton: function (btn) {
        switch (btn) {
            case this.getCE("btnBlue"):
                return RoomConst.CHARACTER_TYPE.BLUE;
            case this.getCE("btnCyan"):
                return RoomConst.CHARACTER_TYPE.CYAN;
            case this.getCE("btnPink"):
                return RoomConst.CHARACTER_TYPE.PINK;
            case this.getCE("btnRed"):
                return RoomConst.CHARACTER_TYPE.RED;
        }
    },

    setSelectedCharacterType: function (v) {
        this._selectedCharacterType = v;
    },
    getSelectedCharacterType: function () {
        return this._selectedCharacterType;
    },

    setCallbackDone: function (v) {
        this._callBackDone = v;
    },
    selectCharacter: function (type) {
        this.setSelectedCharacterType(type);
        this.getCE("pnSelection").x = this.getBtnByCharacterType(type).x;
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnSelect"):
                this._callBackDone();
                break;
            case this.getCE("btnBlue"):
            case this.getCE("btnCyan"):
            case this.getCE("btnPink"):
            case this.getCE("btnRed"):
                this.selectCharacter(this.getCharacterTypeByButton(sender));
                break;
        }
    }
});
