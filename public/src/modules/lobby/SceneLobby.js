
let SceneLobbyLayer = cc.Layer.extend(InjectCCS).extend({
    ctor:function () {
        this._super();
        this.defineCE([
            "scene",
            "lbTitle",
            "btnNewGame",
            "btnSettings",
            "btnGunLibrary",
        ]);
        this.injectElement("zccs/lobby/SceneLobby.json");
        this.init();
    },

    init: function () {
        var rootNode = this.getCE("scene");
        rootNode.setOpacity(0);
        rootNode.runAction(cc.fadeIn(0.3));

        this.getCE("lbTitle").setString(GAME_TITLE);
        this.getCE("btnGunLibrary").setVisible(false);
    },

    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnNewGame"):
                GuiMgr.viewGUI(GuiModeSelector);
                break;
            case this.getCE("btnSettings"):
                GuiMgr.viewGUI(GuiSettings);
                break;
        }
    }
});

let SceneLobby = cc.Scene.extend({
    onEnter:function () {
        this._super();
        let layer = new SceneLobbyLayer();
        this.addChild(layer);
    }
});

