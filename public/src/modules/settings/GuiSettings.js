let GuiSettings = cc.Layer.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "btnExit",
            "btnSave",
            "nd"
        ]);
        this.injectElement("zccs/settings/GuiSettings.json");
        SettingsMgr.loadDataButtonsSettings();
        this.initUserUtils();

        let eventKeyboardListener = cc.EventListener.create({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: (keyCode) => {
                if (this._callbackKeypress) {
                    this._callbackKeypress(keyCode);
                    this._callbackKeypress = null;
                }
            }
        });

        cc.eventManager.addListener(eventKeyboardListener, this);
    },

    setCallbackKeypress: function (v) {
        this._callbackKeypress = v;
    },

    initUserUtils: function () {
        let nd = this.getCE("nd");
        this._userElementArr = [];
        let posX = 0;
        let width = CellButtonSettings.SETTINGS.SIZE.WIDTH;

        let dataArr = SettingsData.getKeySettingsData();

        for (let i = 0; i < 4; i += 1) {
            let userElement = new CellButtonSettings;
            userElement.x = posX;
            posX += width;

            nd.addChild(userElement);
            userElement.setData(dataArr[i]);
            userElement.presentData();
            this._userElementArr.push(userElement);
        }
    },
    scrollViewPresentDataAtIndex: function (idx) {
        this._userElementArr[idx].presentData();
    },
    onTouchEndedEvent: function (sender) {
        switch (sender) {
            case this.getCE("btnExit"):
                GuiMgr.removeGUI(GuiSettings);
                break;
            case this.getCE("btnSave"):
                SettingsData.saveStorage();
                GuiMgr.removeGUI(GuiSettings);
                break;
        }
    }
});

let CellButtonSettings = cc.Node.extend(InjectCCS).extend({
    ctor: function () {
        this._super();
        this.defineCE([
            "lbPlayer",
            "lbLeft",
            "btnLeft",
            "lbDown",
            "btnDown",
            "lbRight",
            "btnRight",
            "lbUp",
            "btnUp",
            "lbShoot",
            "btnShoot",
            "lbBomb",
            "btnBomb"
        ]);
        this.injectElement("zccs/settings/CellButtonSettings.json");
    },

    setData: function (v) {
        this._data = v;
    },
    getData: function () {
        return this._data;
    },

    strKeyFromCharCode: function (key) {
        return keyboardMap[key];
    },
    presentData: function () {
        let data = this.getData();
        this.getCE("lbPlayer").setString("Player " + (data.playerIndex + 1));
        this.getCE("lbLeft").setString(this.strKeyFromCharCode(data.LEFT));
        this.getCE("lbDown").setString(this.strKeyFromCharCode(data.DOWN));
        this.getCE("lbRight").setString(this.strKeyFromCharCode(data.RIGHT));
        this.getCE("lbUp").setString(this.strKeyFromCharCode(data.UP));
        this.getCE("lbShoot").setString(this.strKeyFromCharCode(data.FIRE));
        this.getCE("lbBomb").setString(this.strKeyFromCharCode(data.NADE));
    },

    onTouchEndedEvent: function (sender) {
        let gui = GuiMgr.getGUI(GuiSettings);
        switch (sender) {
            case this.getCE("btnLeft"):
                this.getCE("lbLeft").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().LEFT = keyCode;
                    this.presentData();
                });
                break;
            case this.getCE("btnDown"):
                this.getCE("lbDown").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().DOWN = keyCode;
                    this.presentData();
                });
                break;
            case this.getCE("btnRight"):
                this.getCE("lbRight").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().RIGHT = keyCode;
                    this.presentData();
                });
                break;
            case this.getCE("btnUp"):
                this.getCE("lbUp").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().UP = keyCode;
                    this.presentData();
                });
                break;
            case this.getCE("btnShoot"):
                this.getCE("lbShoot").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().FIRE = keyCode;
                    this.presentData();
                });
                break;
            case this.getCE("btnBomb"):
                this.getCE("lbBomb").setString("...");
                gui.setCallbackKeypress((keyCode) => {
                    this.getData().NADE = keyCode;
                    this.presentData();
                });
                break;
        }
    }
});

CellButtonSettings.SETTINGS = {
    SIZE: {
        WIDTH: 480,
        HEIGHT: 1080
    },
    POSITION_COVER: {
        USER_ACTIVE: 888,
        USER_NONE: 0
    }
};
