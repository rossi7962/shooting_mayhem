var res = {
    font_1: "res/font/Nunito-Black.ttf",

    sprites_gun_0: "sprites/gun/gun_steyr_tmp.png",
    sprites_gun_1: "sprites/gun/gun_ak47.png",
    sprites_gun_2: "sprites/gun/gun_awm.png",
    sprites_gun_3: "sprites/gun/gun_desert_eagle.png",
    sprites_gun_4: "sprites/gun/gun_gatling_gun.png",
    sprites_gun_5: "sprites/gun/gun_glock.png",
    sprites_gun_6: "sprites/gun/gun_m4a1.png",
    sprites_gun_7: "sprites/gun/gun_mp5.png",
    sprites_gun_8: "sprites/gun/gun_schdmit_scout.png",
    sprites_gun_9: "sprites/gun/gun_spass_12.png",

    zccs_lobby_SceneLobby : "zccs/lobby/SceneLobby.json",
    zccs_room_GuiModeSelector: "zccs/room/GuiModeSelector.json",
    zccs_room_GuiMapSelector: "zccs/room/GuiMapSelector.json",
    zccs_room_GuiRoom: "zccs/room/GuiRoom.json",
    zccs_room_CellMap: "zccs/room/CellMap.json",
    zccs_room_CellUserRoom: "zccs/room/CellUserRoom.json",
    zccs_room_CellGunElement: "zccs/room/CellGunElement.json",
    zccs_room_CellGrenade: "zccs/room/CellGrenadeElement.json",
    zccs_room_GuiGunSelect: "zccs/room/GuiGunSelect.json",
    zccs_room_GuiGrenadeSelect: "zccs/room/GuiGrenadeSelect.json",
    zccs_room_GuiCharacterSelection: "zccs/room/GuiCharacterSelect.json",

    zccs_game_SceneGame: "zccs/game/SceneBattle.json",
    zccs_game_CharacterSpriteBattle: "zccs/game/CharacterSpriteBattle.json",
    zccs_game_CharacterDataElement: "zccs/game/CharacterDataElement.json",
    zccs_game_CharacterDataElementGunGame: "zccs/game/CharacterDataElementGunGame.json",
    zccs_game_GuiPauseGame: "zccs/game/GuiPauseGame.json",
    zccs_game_NodeExplode: "zccs/game/NodeExplode.json",
    zccs_game_NodeFlash: "zccs/game/NodeFlash.json",
    zccs_game_NodeSmoke: "zccs/game/NodeSmoke.json",
    zccs_game_NodeTextKill: "zccs/game/NodeTextKill.json",

    zccs_settings_GuiSettings : "zccs/settings/GuiSettings.json",
    zccs_settings_CellButtonSettings : "zccs/settings/CellButtonSettings.json",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
