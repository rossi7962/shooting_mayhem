<GameFile>
  <PropertyGroup Name="GuiModeSelector" Type="Scene" ID="069b4f09-0ec2-49f9-be75-c5e9732fef97" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="11" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="66534736" Tag="17" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="82" G="82" B="82" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="lbTitle" ActionTag="-905071568" Tag="13" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="1120.0000" BottomMargin="978.0000" IsCustomSize="True" FontSize="72" LabelText="Shooting Mayhem   " HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="800.0000" Y="102.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="1.0000" />
            <Position X="1920.0000" Y="1080.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="219" G="213" B="0" />
            <PrePosition X="1.0000" Y="1.0000" />
            <PreSize X="0.4167" Y="0.0944" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <OutlineColor A="255" R="122" G="73" B="17" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lbModeSelect" ActionTag="102997474" Tag="25" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="1120.0000" BottomMargin="978.0000" IsCustomSize="True" FontSize="72" LabelText="Game Mode" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="800.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="400.0000" Y="1080.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="219" G="213" B="0" />
            <PrePosition X="0.2083" Y="1.0000" />
            <PreSize X="0.4167" Y="0.0944" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <OutlineColor A="255" R="122" G="73" B="17" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnGunGame" ActionTag="-793133183" Tag="14" IconVisible="False" RightMargin="1120.0000" TopMargin="394.0000" BottomMargin="586.0000" TouchEnable="True" FontSize="48" ButtonText="Gun Game" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="299" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="800.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="400.0000" Y="636.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2083" Y="0.5889" />
            <PreSize X="0.4167" Y="0.0926" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnLastManStanding" ActionTag="1016379953" Tag="15" IconVisible="False" RightMargin="1120.0000" TopMargin="154.0000" BottomMargin="826.0000" TouchEnable="True" FontSize="48" ButtonText="Last Man Standing" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="401" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="800.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="400.0000" Y="876.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2083" Y="0.8111" />
            <PreSize X="0.4167" Y="0.0926" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnLastTeamStanding" ActionTag="-2044118796" Tag="16" IconVisible="False" RightMargin="1120.0000" TopMargin="274.0000" BottomMargin="706.0000" TouchEnable="True" FontSize="48" ButtonText="Last Team Standing" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="431" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="800.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="400.0000" Y="756.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2083" Y="0.7000" />
            <PreSize X="0.4167" Y="0.0926" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="blue_shooter_1_0" ActionTag="-1313616928" Tag="19" IconVisible="False" LeftMargin="1414.4773" RightMargin="439.5227" TopMargin="675.2387" BottomMargin="332.7613" ctype="SpriteObjectData">
            <Size X="66.0000" Y="72.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_6" ActionTag="400170503" Tag="23" IconVisible="False" LeftMargin="49.4338" RightMargin="-46.4338" TopMargin="20.1223" BottomMargin="15.8777" ctype="SpriteObjectData">
                <Size X="63.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.9338" Y="33.8777" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2263" Y="0.4705" />
                <PreSize X="0.9545" Y="0.5000" />
                <FileData Type="Normal" Path="sprites/gun/gun_desert_eagle.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1447.4773" Y="368.7613" />
            <Scale ScaleX="-5.0000" ScaleY="5.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7539" Y="0.3414" />
            <PreSize X="0.0344" Y="0.0667" />
            <FileData Type="Normal" Path="sprites/character/cyan_shooter.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="blue_shooter_1" ActionTag="-977036364" Tag="18" IconVisible="False" LeftMargin="1668.0084" RightMargin="185.9916" TopMargin="801.6748" BottomMargin="206.3252" ctype="SpriteObjectData">
            <Size X="66.0000" Y="72.0000" />
            <Children>
              <AbstractNodeData Name="spr" ActionTag="1147131403" Tag="20" IconVisible="False" LeftMargin="-2.4001" RightMargin="35.4001" TopMargin="-72.0046" BottomMargin="111.0046" ctype="SpriteObjectData">
                <Size X="33.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="14.0999" Y="127.5046" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2136" Y="1.7709" />
                <PreSize X="0.5000" Y="0.4583" />
                <FileData Type="Normal" Path="sprites/grenade/grenade.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="spr" ActionTag="1125236290" Tag="21" IconVisible="False" LeftMargin="84.0639" RightMargin="-51.0639" TopMargin="-74.7081" BottomMargin="113.7081" ctype="SpriteObjectData">
                <Size X="33.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.5639" Y="130.2081" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.5237" Y="1.8084" />
                <PreSize X="0.5000" Y="0.4583" />
                <FileData Type="Normal" Path="sprites/grenade/grenade.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="spr" ActionTag="-2083064795" Tag="24" IconVisible="False" LeftMargin="42.5402" RightMargin="-9.5402" TopMargin="-84.0004" BottomMargin="123.0004" ctype="SpriteObjectData">
                <Size X="33.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.0402" Y="139.5004" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8945" Y="1.9375" />
                <PreSize X="0.5000" Y="0.4583" />
                <FileData Type="Normal" Path="sprites/grenade/grenade.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="gun_awm_5" ActionTag="900556269" Tag="22" IconVisible="False" LeftMargin="-10.5081" RightMargin="-88.4919" TopMargin="18.9149" BottomMargin="8.0851" ctype="SpriteObjectData">
                <Size X="165.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="71.9919" Y="30.5851" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0908" Y="0.4248" />
                <PreSize X="2.5000" Y="0.6250" />
                <FileData Type="Normal" Path="sprites/gun/gun_awm.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1701.0084" Y="242.3252" />
            <Scale ScaleX="-5.0000" ScaleY="5.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8859" Y="0.2244" />
            <PreSize X="0.0344" Y="0.0667" />
            <FileData Type="Normal" Path="sprites/character/blue_shooter.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="blue_shooter_1_1" ActionTag="-789068354" Tag="27" IconVisible="False" LeftMargin="1324.9398" RightMargin="529.0602" TopMargin="948.1749" BottomMargin="59.8251" ctype="SpriteObjectData">
            <Size X="66.0000" Y="72.0000" />
            <Children>
              <AbstractNodeData Name="gun_awm_5" ActionTag="-2098704705" Tag="31" IconVisible="False" LeftMargin="40.7247" RightMargin="-61.7247" TopMargin="23.7976" BottomMargin="9.2024" ctype="SpriteObjectData">
                <Size X="87.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="84.2247" Y="28.7024" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2761" Y="0.3986" />
                <PreSize X="1.3182" Y="0.5417" />
                <FileData Type="Normal" Path="sprites/gun/gun_steyr_tmp.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1357.9398" Y="95.8251" />
            <Scale ScaleX="-5.0000" ScaleY="5.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7073" Y="0.0887" />
            <PreSize X="0.0344" Y="0.0667" />
            <FileData Type="Normal" Path="sprites/character/pinky_shooter.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnExit" ActionTag="-85907924" Tag="26" IconVisible="False" RightMargin="1120.0000" TopMargin="980.0000" TouchEnable="True" FontSize="48" ButtonText="Back To Lobby" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="314" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="800.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="400.0000" Y="50.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2083" Y="0.0463" />
            <PreSize X="0.4167" Y="0.0926" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btnContinue" ActionTag="-957984882" Tag="52" IconVisible="False" RightMargin="1120.0000" TopMargin="880.0000" BottomMargin="100.0000" TouchEnable="True" FontSize="48" ButtonText="Continue" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="179" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="800.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="400.0000" Y="150.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2083" Y="0.1389" />
            <PreSize X="0.4167" Y="0.0926" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>