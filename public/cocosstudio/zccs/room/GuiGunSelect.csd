<GameFile>
  <PropertyGroup Name="GuiGunSelect_0" Type="Scene" ID="b792d8a1-4702-42ff-9b6e-80f519d7f6da" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="74" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="pnFog" ActionTag="-104171955" Tag="75" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-0.0001" RightMargin="0.0001" TopMargin="-1.9751" BottomMargin="1.9751" TouchEnable="True" ClipAble="False" BackColorAlpha="127" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <AnchorPoint />
            <Position X="-0.0001" Y="1.9751" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.0018" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_2" ActionTag="1367720014" Tag="76" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="560.0000" RightMargin="560.0000" TopMargin="140.0000" BottomMargin="140.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="800.0000" Y="800.0000" />
            <Children>
              <AbstractNodeData Name="lbTitle" ActionTag="907843399" Tag="82" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="181.5000" RightMargin="181.5000" TopMargin="7.5000" BottomMargin="727.5000" FontSize="48" LabelText="Handgun Selection" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="437.0000" Y="65.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="760.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9500" />
                <PreSize X="0.5462" Y="0.0812" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="svGunList" ActionTag="-2004630889" Tag="77" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="100.0000" RightMargin="100.0000" TopMargin="100.0000" BottomMargin="500.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Horizontal" ctype="ScrollViewObjectData">
                <Size X="600.0000" Y="200.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="600.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7500" />
                <PreSize X="0.7500" Y="0.2500" />
                <SingleColor A="255" R="255" G="150" B="100" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="600" Height="200" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnSelect" ActionTag="1837536949" Tag="83" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="250.0000" RightMargin="250.0000" TopMargin="694.9450" BottomMargin="5.0550" TouchEnable="True" FontSize="48" ButtonText="Done" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="5" BottomEage="5" Scale9OriginX="15" Scale9OriginY="5" Scale9Width="93" Scale9Height="55" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="300.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="55.0550" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0688" />
                <PreSize X="0.3750" Y="0.1250" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="prDamage" ActionTag="1705400577" Tag="84" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="325.0000" BottomMargin="445.0000" ProgressInfo="35" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="1296091311" Tag="85" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Damage" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="460.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.5750" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="prWeight" ActionTag="-565999540" Tag="86" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="375.0000" BottomMargin="395.0000" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="-1462743106" Tag="87" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Weight" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="410.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.5125" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="prRecoil" ActionTag="324422373" Tag="88" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="425.0000" BottomMargin="345.0000" ProgressInfo="49" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="-835130154" Tag="89" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Recoil" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="360.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.4500" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="prAttackDelay" ActionTag="-1878689713" Tag="90" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="475.0000" BottomMargin="295.0000" ProgressInfo="19" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="-235273641" Tag="91" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Attack Delay" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="310.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.3875" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="prAmmo" ActionTag="686117335" Tag="92" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="525.0000" BottomMargin="245.0000" ProgressInfo="68" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="1488829388" Tag="93" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Ammo" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="260.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.3250" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="prReloadTime" ActionTag="1348217126" Tag="94" IconVisible="False" LeftMargin="300.0000" RightMargin="100.0000" TopMargin="575.0000" BottomMargin="195.0000" ProgressInfo="48" ctype="LoadingBarObjectData">
                <Size X="400.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="lbText" ActionTag="-686459867" Tag="95" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-200.0082" RightMargin="440.0082" TopMargin="1.5000" BottomMargin="1.5000" IsCustomSize="True" FontSize="24" LabelText="Reload Time" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="27.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-200.0082" Y="15.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.5000" Y="0.5000" />
                    <PreSize X="0.4000" Y="0.9000" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="500.0000" Y="210.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6250" Y="0.2625" />
                <PreSize X="0.5000" Y="0.0375" />
                <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbGunName" ActionTag="-1244711686" Tag="100" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="100.0000" RightMargin="100.0000" TopMargin="619.5000" BottomMargin="115.5000" IsCustomSize="True" FontSize="32" LabelText="Desert Eagle" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="600.0000" Y="65.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="148.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1850" />
                <PreSize X="0.7500" Y="0.0812" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.0000" Y="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.4167" Y="0.7407" />
            <SingleColor A="255" R="77" G="77" B="77" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>