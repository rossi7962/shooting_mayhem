<GameFile>
  <PropertyGroup Name="GuiMapSelector" Type="Scene" ID="336cc64a-6b7d-4358-8431-0423c824b969" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="34" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-1636276804" Tag="35" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <Children>
              <AbstractNodeData Name="svMapList" ActionTag="1913438455" Tag="36" IconVisible="False" LeftMargin="100.0000" RightMargin="1220.0000" TopMargin="100.0000" BottomMargin="180.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="600.0000" Y="800.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="100.0000" Y="980.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0521" Y="0.9074" />
                <PreSize X="0.3125" Y="0.7407" />
                <SingleColor A="255" R="77" G="77" B="77" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="600" Height="880" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbTitle" ActionTag="-1916388608" Tag="46" IconVisible="False" LeftMargin="920.0000" RightMargin="200.0000" TopMargin="100.0000" BottomMargin="878.0000" IsCustomSize="True" FontSize="72" LabelText="Shooting Mayhem   " HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="7" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="800.0000" Y="102.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="1320.0000" Y="980.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="219" G="213" B="0" />
                <PrePosition X="0.6875" Y="0.9074" />
                <PreSize X="0.4167" Y="0.0944" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="122" G="73" B="17" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pnMapReview" ActionTag="1704355199" Tag="37" IconVisible="False" LeftMargin="820.0000" RightMargin="100.0000" TopMargin="300.0000" BottomMargin="280.0000" TouchEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="1000.0000" Y="500.0000" />
                <Children>
                  <AbstractNodeData Name="lbQuestionMark" ActionTag="2078236164" Tag="45" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="450.0000" RightMargin="450.0000" TopMargin="149.0000" BottomMargin="249.0000" IsCustomSize="True" FontSize="72" LabelText="?" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="500.0000" Y="300.0000" />
                    <Scale ScaleX="4.0000" ScaleY="4.0000" />
                    <CColor A="255" R="219" G="213" B="0" />
                    <PrePosition X="0.5000" Y="0.6000" />
                    <PreSize X="0.1000" Y="0.2040" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="122" G="73" B="17" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lbRandomMap" ActionTag="-64534704" Tag="53" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="100.0000" RightMargin="100.0000" TopMargin="336.0000" BottomMargin="62.0000" IsCustomSize="True" FontSize="72" LabelText="Random Map   " HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="7" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="800.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="500.0000" Y="164.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="219" G="213" B="0" />
                    <PrePosition X="0.5000" Y="0.3280" />
                    <PreSize X="0.8000" Y="0.2040" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="122" G="73" B="17" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="1320.0000" Y="780.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6875" Y="0.7222" />
                <PreSize X="0.5208" Y="0.4630" />
                <SingleColor A="255" R="77" G="77" B="77" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnExit" ActionTag="1825518898" Tag="42" IconVisible="False" RightMargin="1120.0000" TopMargin="970.0000" BottomMargin="10.0000" TouchEnable="True" FontSize="48" ButtonText="Back To Mode Select" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="283" Scale9Height="43" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="800.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="400.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2083" Y="0.0556" />
                <PreSize X="0.4167" Y="0.0926" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnRandom" ActionTag="381020533" Tag="43" IconVisible="False" LeftMargin="920.0000" RightMargin="200.0000" TopMargin="850.0000" BottomMargin="130.0000" TouchEnable="True" FontSize="48" ButtonText="Random Map" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="283" Scale9Height="43" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="800.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1320.0000" Y="180.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6875" Y="0.1667" />
                <PreSize X="0.4167" Y="0.0926" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="btnContinue" ActionTag="1313434601" Tag="44" IconVisible="False" LeftMargin="920.0000" RightMargin="200.0000" TopMargin="957.0645" BottomMargin="22.9355" TouchEnable="True" FontSize="48" ButtonText="Continue" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="283" Scale9Height="43" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="800.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="1320.0000" Y="72.9355" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6875" Y="0.0675" />
                <PreSize X="0.4167" Y="0.0926" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="238" G="177" B="107" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>