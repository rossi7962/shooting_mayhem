<GameFile>
  <PropertyGroup Name="SceneLobby" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="1920.0000" Y="1080.0000" />
        <Children>
          <AbstractNodeData Name="pn" ActionTag="-1329854944" Tag="5" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TouchEnable="True" StretchWidthEnable="True" StretchHeightEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="1920.0000" Y="1080.0000" />
            <Children>
              <AbstractNodeData Name="scene" ActionTag="953446860" Tag="5" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BothEdge" LeftMargin="-440.8320" RightMargin="440.8320" TopMargin="377.2440" BottomMargin="-377.2440" ctype="SpriteObjectData">
                <Size X="1920.0000" Y="1080.0000" />
                <Children>
                  <AbstractNodeData Name="lbTitle" ActionTag="392761011" Tag="4" IconVisible="False" LeftMargin="846.8623" RightMargin="273.1377" TopMargin="25.4232" BottomMargin="952.5768" IsCustomSize="True" FontSize="64" LabelText="Shooting Mayhem&#xA;" HorizontalAlignmentType="HT_Right" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="800.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1246.8623" Y="1003.5768" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="219" G="213" B="0" />
                    <PrePosition X="0.6494" Y="0.9292" />
                    <PreSize X="0.4167" Y="0.0944" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="122" G="73" B="17" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnNewGame" ActionTag="1508325319" Tag="6" IconVisible="False" LeftMargin="1330.0000" RightMargin="290.0000" TopMargin="260.0000" BottomMargin="740.0000" TouchEnable="True" FontSize="48" ButtonText="Play Now" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="193" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="300.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1480.0000" Y="780.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7708" Y="0.7222" />
                    <PreSize X="0.1563" Y="0.0741" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnSettings" ActionTag="751645612" Tag="9" IconVisible="False" LeftMargin="1330.0000" RightMargin="290.0000" TopMargin="340.0000" BottomMargin="660.0000" TouchEnable="True" FontSize="48" ButtonText="Settings" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="164" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="300.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1480.0000" Y="700.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7708" Y="0.6481" />
                    <PreSize X="0.1563" Y="0.0741" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="btnGunLibrary" ActionTag="1171721657" Tag="10" IconVisible="False" LeftMargin="1330.0000" RightMargin="290.0000" TopMargin="420.0000" BottomMargin="580.0000" TouchEnable="True" FontSize="48" ButtonText="Gun Library" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="15" Scale9OriginY="6" Scale9Width="244" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="300.0000" Y="80.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="1480.0000" Y="620.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7708" Y="0.5741" />
                    <PreSize X="0.1563" Y="0.0741" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <TextColor A="255" R="255" G="255" B="255" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.4989" ScaleY="0.6515" />
                <Position X="517.0560" Y="326.3760" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2693" Y="0.3022" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="sprites/map/demo_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="960.0000" Y="540.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>