<GameFile>
  <PropertyGroup Name="CharacterSpriteBattle" Type="Node" ID="f81702c0-294f-4baf-a9d1-22a31d350153" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="284" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="lbName" ActionTag="1934583901" Tag="346" IconVisible="False" LeftMargin="-100.0000" RightMargin="-100.0000" TopMargin="-99.5000" BottomMargin="76.5000" IsCustomSize="True" FontSize="20" LabelText="Phu Thuy Tro Tan" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="200.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="88.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lbStatus" ActionTag="-1936040158" Tag="25" IconVisible="False" LeftMargin="-100.0000" RightMargin="-100.0000" TopMargin="-139.5000" BottomMargin="116.5000" IsCustomSize="True" FontSize="16" LabelText="Reloading" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="200.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="128.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
            <OutlineColor A="255" R="26" G="26" B="26" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="prReload" ActionTag="1966839949" Tag="360" IconVisible="False" LeftMargin="-30.0000" RightMargin="-30.0000" TopMargin="-114.0000" BottomMargin="102.0000" ctype="LoadingBarObjectData">
            <Size X="60.0000" Y="12.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="108.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="sprCharacter" ActionTag="1904249327" Tag="285" IconVisible="False" LeftMargin="-33.0000" RightMargin="-33.0000" TopMargin="-72.0000" ctype="SpriteObjectData">
            <Size X="66.0000" Y="72.0000" />
            <Children>
              <AbstractNodeData Name="foot1" ActionTag="769720706" Tag="16" IconVisible="False" LeftMargin="4.0000" RightMargin="41.0000" TopMargin="60.0000" ctype="SpriteObjectData">
                <Size X="21.0000" Y="12.0000" />
                <AnchorPoint />
                <Position X="4.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0606" />
                <PreSize X="0.3182" Y="0.1667" />
                <FileData Type="Normal" Path="sprites/character/foot.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="foot2" ActionTag="-1677040983" Tag="17" IconVisible="False" LeftMargin="30.0000" RightMargin="15.0000" TopMargin="60.0000" ctype="SpriteObjectData">
                <Size X="21.0000" Y="12.0000" />
                <AnchorPoint />
                <Position X="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4545" />
                <PreSize X="0.3182" Y="0.1667" />
                <FileData Type="Normal" Path="sprites/character/foot.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprWeapon" ActionTag="2107624537" Tag="286" IconVisible="False" LeftMargin="56.0000" RightMargin="-38.0000" TopMargin="25.0000" BottomMargin="17.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.2121" Y="0.4444" />
                <PreSize X="0.7273" Y="0.4167" />
                <FileData Type="Normal" Path="sprites/gun/gun_glock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprHit" ActionTag="-1671378348" Alpha="119" Tag="7" IconVisible="False" LeftMargin="-75.0204" RightMargin="75.0204" ctype="SpriteObjectData">
                <Size X="66.0000" Y="72.0000" />
                <AnchorPoint />
                <Position X="-75.0204" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-1.1367" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="sprites/character/shooter_hit.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="sprites/character/red_shooter.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sprFire" ActionTag="50350638" Tag="30" IconVisible="False" LeftMargin="77.0916" RightMargin="-107.0916" TopMargin="-72.0049" BottomMargin="48.0049" ctype="SpriteObjectData">
            <Size X="30.0000" Y="24.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="77.0916" Y="60.0049" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="sprites/effect/fire_2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ndGen" ActionTag="-311387769" Tag="345" IconVisible="True" LeftMargin="80.0000" RightMargin="-80.0000" TopMargin="-41.0000" BottomMargin="41.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="80.0000" Y="41.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>