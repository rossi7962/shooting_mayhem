<GameFile>
  <PropertyGroup Name="NodeTextKill" Type="Node" ID="fdae460d-2b76-48da-8f0c-e670d8881487" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="21" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="nd" ActionTag="1014144792" Tag="53" IconVisible="True" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="lbKiller" ActionTag="-1082212581" Tag="22" IconVisible="False" RightMargin="-500.0000" TopMargin="-61.5000" BottomMargin="2.5000" IsCustomSize="True" FontSize="36" LabelText="abcasd" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="500.0000" Y="59.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="238" G="255" B="254" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprWeapon" ActionTag="-1444106690" Tag="23" IconVisible="False" LeftMargin="276.0000" RightMargin="-324.0000" TopMargin="-51.0000" BottomMargin="21.0000" ctype="SpriteObjectData">
                <Size X="48.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.0000" Y="36.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="sprites/gun/gun_glock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbDeath" ActionTag="-1021764897" Tag="24" IconVisible="False" LeftMargin="399.6838" RightMargin="-899.6838" TopMargin="-61.5000" BottomMargin="2.5000" IsCustomSize="True" FontSize="36" LabelText="abcasd zxczxc" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="500.0000" Y="59.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="399.6838" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="238" G="255" B="254" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="26" B="26" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprSelfDestruct" ActionTag="1315942026" Tag="52" IconVisible="False" LeftMargin="24.0000" RightMargin="-74.0000" TopMargin="-57.0000" BottomMargin="7.0000" ctype="SpriteObjectData">
                <Size X="50.0000" Y="50.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="24.0000" Y="32.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="sprites/effect/self_destruct.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>