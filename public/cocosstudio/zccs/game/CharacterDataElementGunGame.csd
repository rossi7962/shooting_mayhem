<GameFile>
  <PropertyGroup Name="CharacterDataElementGunGame" Type="Layer" ID="351b2268-a03d-496b-9fc0-ae636792d568" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="259" ctype="GameLayerObjectData">
        <Size X="480.0000" Y="200.0000" />
        <Children>
          <AbstractNodeData Name="pnContainer" ActionTag="1744916587" Tag="262" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="20.0000" RightMargin="20.0000" TopMargin="20.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="440.0000" Y="160.0000" />
            <Children>
              <AbstractNodeData Name="pnAvatar" ActionTag="1978367289" Tag="261" IconVisible="False" LeftMargin="20.0000" RightMargin="300.0000" TopMargin="20.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="True" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="120.0000" Y="120.0000" />
                <Children>
                  <AbstractNodeData Name="sprCharacter" ActionTag="775450315" Tag="263" IconVisible="False" LeftMargin="30.3432" RightMargin="23.6568" TopMargin="76.5949" BottomMargin="-28.5949" ctype="SpriteObjectData">
                    <Size X="66.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="63.3432" Y="7.4051" />
                    <Scale ScaleX="2.6000" ScaleY="2.6000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5279" Y="0.0617" />
                    <PreSize X="0.5500" Y="0.6000" />
                    <FileData Type="Normal" Path="sprites/character/blue_shooter.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="20.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0455" Y="0.1250" />
                <PreSize X="0.2727" Y="0.7500" />
                <SingleColor A="255" R="191" G="191" B="191" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbName" ActionTag="1578493608" Tag="264" IconVisible="False" LeftMargin="156.0000" RightMargin="24.0000" TopMargin="20.5000" BottomMargin="116.5000" IsCustomSize="True" FontSize="20" LabelText="Phu Thuy Tro Tan" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="260.0000" Y="23.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="156.0000" Y="128.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="26" G="26" B="26" />
                <PrePosition X="0.3545" Y="0.8000" />
                <PreSize X="0.5909" Y="0.1437" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="divider" ActionTag="1059620409" Tag="267" IconVisible="False" LeftMargin="156.0000" RightMargin="8.0000" TopMargin="46.7759" BottomMargin="105.2241" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="276.0000" Y="8.0000" />
                <AnchorPoint />
                <Position X="156.0000" Y="105.2241" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3545" Y="0.6577" />
                <PreSize X="0.6273" Y="0.0500" />
                <SingleColor A="255" R="191" G="191" B="191" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="divider" ActionTag="2010897114" Tag="268" IconVisible="False" LeftMargin="246.1015" RightMargin="185.8985" TopMargin="51.4769" BottomMargin="16.5231" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="8.0000" Y="92.0000" />
                <AnchorPoint />
                <Position X="246.1015" Y="16.5231" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5593" Y="0.1033" />
                <PreSize X="0.0182" Y="0.5750" />
                <SingleColor A="255" R="191" G="191" B="191" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="lb" ActionTag="-1316456446" Tag="265" IconVisible="False" LeftMargin="170.0000" RightMargin="218.0000" TopMargin="60.5000" BottomMargin="72.5000" FontSize="20" LabelText="Level" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="52.0000" Y="27.0000" />
                <Children>
                  <AbstractNodeData Name="lbLevel" ActionTag="1760275649" Tag="266" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-5.0000" RightMargin="-5.0000" TopMargin="28.5000" BottomMargin="-70.5000" FontSize="48" LabelText="10" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="62.0000" Y="69.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="26.0000" Y="-36.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="168" G="12" B="12" />
                    <PrePosition X="0.5000" Y="-1.3333" />
                    <PreSize X="1.1923" Y="2.5556" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="196.0000" Y="86.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4455" Y="0.5375" />
                <PreSize X="0.1182" Y="0.1688" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lbGunName" ActionTag="1906567875" Tag="269" IconVisible="False" LeftMargin="261.0000" RightMargin="13.0000" TopMargin="60.5000" BottomMargin="72.5000" IsCustomSize="True" FontSize="20" LabelText="AWM" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="166.0000" Y="27.0000" />
                <Children>
                  <AbstractNodeData Name="lbAmmo" ActionTag="-412759893" Tag="270" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="23.0000" RightMargin="23.0000" TopMargin="28.5000" BottomMargin="-70.5000" IsCustomSize="True" FontSize="48" LabelText="10" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="120.0000" Y="69.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="83.0000" Y="-36.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="244" B="48" />
                    <PrePosition X="0.5000" Y="-1.3333" />
                    <PreSize X="0.7229" Y="2.5556" />
                    <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="344.0000" Y="86.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.7818" Y="0.5375" />
                <PreSize X="0.3773" Y="0.1688" />
                <FontResource Type="Normal" Path="font/Nunito-Black.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="240.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9167" Y="0.8000" />
            <SingleColor A="255" R="255" G="255" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>