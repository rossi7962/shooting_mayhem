let path = require('path');
let express = require('express');
let app = express();
let http = require('http').createServer(app);
let io = require('socket.io')(http);

let gamePath = path.join(__dirname, 'public');
app.use(express.static(gamePath));

io.on('connection', (socket) => {
    console.log('a user connected');
});

let port = process.env.PORT || 8080;
http.listen(port, () => {
    console.log('listening on port: ' + port);
});

// heroku